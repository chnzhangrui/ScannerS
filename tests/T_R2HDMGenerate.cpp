#include "ScannerS/Models/R2HDM.hpp"

#include "ScannerS/Constants.hpp"
#include "ScannerS/Utilities.hpp"
#include "catch.hpp"

TEST_CASE("R2HDM Generation", "[R2HDM][unit]") {
  using namespace ScannerS::Models;
  SECTION("Correct ordering") {
    R2HDM::AngleInput in{125.09,
                         100,
                         200,
                         200,
                         1.24031081,
                         2.5,
                         2000,
                         R2HDM::Yuk::typeI,
                         ScannerS::Constants::vEW};
    R2HDM::ParameterPoint p{in};
    REQUIRE(p.mHl == 100);
    REQUIRE(p.mHh == 125.09);
    REQUIRE(p.mA == 200);
    REQUIRE(p.mHp == 200);
    REQUIRE(p.alpha == 1.24031081);
    REQUIRE(p.tbeta == 2.5);
    REQUIRE(p.m12sq == 2000);

    CHECK(p.L[0] == Approx(6.69060770e-01));
    CHECK(p.L[1] == Approx(2.72715608e-01));
    CHECK(p.L[2] == Approx(1.30684681e+00));
    CHECK(p.L[3] == Approx(-5.64127725e-01));
    CHECK(p.L[4] == Approx(-5.64127725e-01));

    CHECK(p.m11sq == Approx(-2464.1668706409));
    CHECK(p.m22sq == Approx(-7073.0991108036));
  }
  SECTION("Inverted Ordering") {
    R2HDM::AngleInput in{100,
                         125.09,
                         200,
                         200,
                         -0.330486,
                         2.5,
                         2000,
                         R2HDM::Yuk::typeI,
                         ScannerS::Constants::vEW};
    R2HDM::ParameterPoint p{in};
    REQUIRE(p.mHl == 100);
    REQUIRE(p.mHh == 125.09);
    REQUIRE(p.mA == 200);
    REQUIRE(p.mHp == 200);
    REQUIRE(p.alpha == Approx(1.24031081));
    REQUIRE(p.tbeta == 2.5);
    REQUIRE(p.m12sq == 2000);

    CHECK(p.L[0] == Approx(6.69060770e-01));
    CHECK(p.L[1] == Approx(2.72715608e-01));
    CHECK(p.L[2] == Approx(1.30684681e+00));
    CHECK(p.L[3] == Approx(-5.64127725e-01));
    CHECK(p.L[4] == Approx(-5.64127725e-01));

    CHECK(p.m11sq == Approx(-2464.1668706409));
    CHECK(p.m22sq == Approx(-7073.0991108036));
  }

  SECTION("Physical Input") {
    R2HDM::PhysicalInput in{125.09,
                            100,
                            200,
                            200,
                            -0.05,
                            2.5,
                            2000,
                            R2HDM::Yuk::typeI,
                            ScannerS::Constants::vEW};
    R2HDM::ParameterPoint p{in};
    REQUIRE(p.mHl == in.mHb);
    REQUIRE(p.mHh == in.mHa);
    REQUIRE(p.mA == in.mA);
    REQUIRE(p.mHp == in.mHp);
    REQUIRE(p.alpha == Approx(1.24031081));
    REQUIRE(p.tbeta == in.tbeta);
    REQUIRE(p.m12sq == in.m12sq);

    CHECK(p.L[0] == Approx(6.69060770e-01));
    CHECK(p.L[1] == Approx(2.72715608e-01));
    CHECK(p.L[2] == Approx(1.30684681e+00));
    CHECK(p.L[3] == Approx(-5.64127725e-01));
    CHECK(p.L[4] == Approx(-5.64127725e-01));

    CHECK(p.m11sq == Approx(-2464.1668706409));
    CHECK(p.m22sq == Approx(-7073.0991108036));

    CHECK(std::sin(std::atan(p.tbeta) - p.alpha) == Approx(-0.05));
  }

  SECTION("Physical Inverted Ordering") {
    R2HDM::PhysicalInput in{100,
                            125.09,
                            200,
                            200,
                            -9.98749218e-01,
                            2.5,
                            2000,
                            R2HDM::Yuk::typeI,
                            ScannerS::Constants::vEW};
    R2HDM::ParameterPoint p{in};
    REQUIRE(p.mHl == in.mHa);
    REQUIRE(p.mHh == in.mHb);
    REQUIRE(p.mA == in.mA);
    REQUIRE(p.mHp == in.mHp);
    REQUIRE(p.alpha == Approx(1.24031081));
    REQUIRE(p.tbeta == in.tbeta);
    REQUIRE(p.m12sq == in.m12sq);

    CHECK(p.L[0] == Approx(6.69060770e-01));
    CHECK(p.L[1] == Approx(2.72715608e-01));
    CHECK(p.L[2] == Approx(1.30684681e+00));
    CHECK(p.L[3] == Approx(-5.64127725e-01));
    CHECK(p.L[4] == Approx(-5.64127725e-01));

    CHECK(p.m11sq == Approx(-2464.1668706409));
    CHECK(p.m22sq == Approx(-7073.0991108036));
    CHECK(std::sin(std::atan(p.tbeta) - p.alpha) == Approx(-0.05));
  }

  SECTION("cVV sign check") {
    const auto mHl = 125.09;
    const auto mHh = 729.2685294782342;
    const auto c_HbVV = 0.11114245702744859;
    for (auto tbeta : {0.1, 1., 1.8671612306148408, 20.}) {
      const auto basePoint = R2HDM::PhysicalInput{-1,
                                                  -1,
                                                  812.72222261082163,
                                                  730.78376012588205,
                                                  0.,
                                                  tbeta,
                                                  152378.41465252597,
                                                  R2HDM::Yuk::typeII,
                                                  ScannerS::Constants::vEW};
      INFO("tbeta = " << tbeta);
      auto plusReorderIn = basePoint;
      plusReorderIn.mHa = mHl;
      plusReorderIn.mHb = mHh;
      plusReorderIn.c_HbVV = c_HbVV;

      auto plusOrderedIn = plusReorderIn;
      std::swap(plusOrderedIn.mHa, plusOrderedIn.mHb);

      auto minusReorderIn = plusReorderIn;
      minusReorderIn.c_HbVV *= -1;

      auto minusOrderedIn = plusOrderedIn;
      minusOrderedIn.c_HbVV *= -1;

      auto plus1mReorderIn = plusReorderIn;
      plus1mReorderIn.c_HbVV = std::sqrt(1 - std::pow(c_HbVV, 2));

      auto plus1mOrderedIn = plusOrderedIn;
      plus1mOrderedIn.c_HbVV = std::sqrt(1 - std::pow(c_HbVV, 2));

      auto minus1mReorderIn = plus1mReorderIn;
      minus1mReorderIn.c_HbVV *= -1;

      auto minus1mOrderedIn = plus1mOrderedIn;
      minus1mOrderedIn.c_HbVV *= -1;

      auto ins = std::vector{plusReorderIn,    plusOrderedIn,   minusReorderIn,
                             minusOrderedIn,   plus1mReorderIn, plus1mOrderedIn,
                             minus1mReorderIn, minus1mOrderedIn};

      auto ps = std::vector<R2HDM::ParameterPoint>{};
      for (auto &in : ins)
        ps.emplace_back(in);

      for (std::size_t i = 0; i != ins.size(); ++i) {
        INFO(i);
        REQUIRE(ps[i].mHl == std::min(ins[i].mHa, ins[i].mHb));
        REQUIRE(ps[i].mHh == std::max(ins[i].mHa, ins[i].mHb));
        REQUIRE(ps[i].tbeta == ins[i].tbeta);
        R2HDM::CalcCouplings(ps[i]);
      }

      auto equivalentCases = std::vector<std::pair<std::size_t, std::size_t>>{
          {0, 5}, {1, 4}, {2, 7}, {3, 6}};

      for (auto [i, j] : equivalentCases) {
        INFO(i << ", " << j << " alpha: " << ps[i].alpha);
        CHECK(ps[i].alpha == Approx(ps[j].alpha));
        CHECK(ps[i].data["c_HlVV"] == Approx(ps[j].data["c_HlVV"]));
        CHECK(ps[i].data["c_HhVV"] == Approx(ps[j].data["c_HhVV"]));
        CHECK(ps[i].data["c_Hluu_e"] == Approx(ps[j].data["c_Hluu_e"]));
        CHECK(ps[i].data["c_Hhuu_e"] == Approx(ps[j].data["c_Hhuu_e"]));
        CHECK(ps[i].data["c_Hldd_e"] == Approx(ps[j].data["c_Hldd_e"]));
        CHECK(ps[i].data["c_Hhdd_e"] == Approx(ps[j].data["c_Hhdd_e"]));
        if (ins[i].mHa >= ins[i].mHb) {
          CHECK(ps[i].data["c_HlVV"] ==
                ((ins[i].c_HbVV < 0 &&
                  -std::sqrt(1 - std::pow(ins[i].c_HbVV, 2)) / ins[i].c_HbVV <
                      ins[i].tbeta)
                     ? -Approx(ins[i].c_HbVV)
                     : Approx(ins[i].c_HbVV)));
        } else {
          CHECK(ps[i].data["c_HhVV"] ==
                (ins[i].tbeta < -ins[i].c_HbVV /
                                    std::sqrt(1 - std::pow(ins[i].c_HbVV, 2))
                     ? -Approx(ins[i].c_HbVV)
                     : Approx(ins[i].c_HbVV)));
        }
        if (ins[j].mHa >= ins[j].mHb) {
          CHECK(ps[j].data["c_HlVV"] ==
                ((ins[j].c_HbVV < 0 &&
                  -std::sqrt(1 - std::pow(ins[j].c_HbVV, 2)) / ins[j].c_HbVV <
                      ins[j].tbeta)
                     ? -Approx(ins[j].c_HbVV)
                     : Approx(ins[j].c_HbVV)));
        } else {
          CHECK(ps[j].data["c_HhVV"] ==
                (ins[j].tbeta < -ins[j].c_HbVV /
                                    std::sqrt(1 - std::pow(ins[j].c_HbVV, 2))
                     ? -Approx(ins[j].c_HbVV)
                     : Approx(ins[j].c_HbVV)));
        }
      }
    }
  }
}
