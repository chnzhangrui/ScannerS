#include "ScannerS/Models/R2HDM.hpp"
#include "AnyHdecay.hpp"
#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Interfaces/HiggsBoundsSignals.hpp"
#include "ScannerS/Tools/SushiTables.hpp"
#include "ScannerS/Utilities.hpp"
#include <Eigen/Core>
#include <algorithm>
#include <cmath>
#include <map>
#include <sstream>
#include <utility>

namespace ScannerS::Models {

// -------------------------------- Generation --------------------------------
namespace {
double alphaFromcHbVV(double c_HbVV, double tbeta) {
  const auto cb = std::cos(std::atan(tbeta));
  const auto sb = std::sin(std::atan(tbeta));
  const auto c_HaVV = std::sqrt(1 - std::pow(c_HbVV, 2));

  return std::asin(Utilities::sign(c_HaVV * cb + c_HbVV * sb) *
                   (c_HaVV * sb - c_HbVV * cb));
}

double CalcSortedAlpha(double mHa, double mHb, double alphaIn) {
  using ScannerS::Constants::pi;
  if (mHa >= mHb)
    return alphaIn;
  else {
    if (sin(alphaIn) > 0)
      return alphaIn - pi / 2;
    else
      return alphaIn + pi / 2;
  }
}

std::array<double, 5> CalcLambdas(double mHl, double mHh, double mA, double mHp,
                                  double tbeta, double alpha, double v,
                                  double m12sq) {
  using std::atan;
  using std::cos;
  using std::pow;
  using std::sin;
  double L1 =
      ((1 + pow(tbeta, 2)) * (pow(mHh, 2) + pow(mHl, 2) - 2 * m12sq * tbeta +
                              (mHh - mHl) * (mHh + mHl) * cos(2 * alpha))) /
      (2. * pow(v, 2));
  double L2 =
      ((1 + pow(tbeta, 2)) * (pow(mHh, 2) + pow(mHl, 2) - (2 * m12sq) / tbeta +
                              (-pow(mHh, 2) + pow(mHl, 2)) * cos(2 * alpha))) /
      (2. * pow(tbeta, 2) * pow(v, 2));
  double L3 = (2 * pow(mHp, 2) +
               1 / sin(2 * atan(tbeta)) *
                   (-2 * m12sq + (mHh - mHl) * (mHh + mHl) * sin(2 * alpha))) /
              pow(v, 2);
  double L4 =
      (pow(mA, 2) - 2 * pow(mHp, 2) + m12sq * (1 / tbeta + tbeta)) / pow(v, 2);
  double L5 = (m12sq - pow(mA, 2) * tbeta + m12sq * pow(tbeta, 2)) /
              (tbeta * pow(v, 2));
  return {L1, L2, L3, L4, L5};
}

double CalcM11sq(double tbeta, double m12sq, double v,
                 const std::array<double, 5> &L) {
  return (2 * m12sq * (tbeta + pow(tbeta, 3)) -
          pow(v, 2) * (L[0] + pow(tbeta, 2) * (L[2] + L[3] + L[4]))) /
         (2. * (1 + pow(tbeta, 2)));
}

double CalcM22sq(double tbeta, double m12sq, double v,
                 const std::array<double, 5> &L) {
  return m12sq / tbeta -
         (pow(v, 2) * (pow(tbeta, 2) * L[1] + L[2] + L[3] + L[4])) /
             (2. * (1 + pow(tbeta, 2)));
}

} // namespace

R2HDM::ParameterPoint::ParameterPoint(const AngleInput &in)
    : mHl{std::min({in.mHa, in.mHb})}, mHh{std::max({in.mHa, in.mHb})},
      mA{in.mA}, mHp{in.mHp}, tbeta{in.tbeta}, m12sq{in.m12sq},
      alpha{CalcSortedAlpha(in.mHa, in.mHb, in.alpha)},
      L{CalcLambdas(mHl, mHh, mA, mHp, tbeta, alpha, in.v, m12sq)},
      m11sq{CalcM11sq(tbeta, m12sq, in.v, L)},
      m22sq{CalcM22sq(tbeta, m12sq, in.v, L)}, type{in.type}, v{in.v} {}

R2HDM::ParameterPoint::ParameterPoint(const PhysicalInput &in)
    : mHl{std::min({in.mHa, in.mHb})}, mHh{std::max({in.mHa, in.mHb})},
      mA{in.mA}, mHp{in.mHp}, tbeta{in.tbeta}, m12sq{in.m12sq},
      alpha{CalcSortedAlpha(
          in.mHa, in.mHb,
          alphaFromcHbVV(in.mHb > in.mHa ? -in.c_HbVV : in.c_HbVV, tbeta))},
      L{CalcLambdas(mHl, mHh, mA, mHp, tbeta, alpha, in.v, m12sq)},
      m11sq{CalcM11sq(tbeta, m12sq, in.v, L)},
      m22sq{CalcM22sq(tbeta, m12sq, in.v, L)}, type{in.type}, v{in.v} {}

// -------------------------------- Pheno --------------------------------
void R2HDM::RunHdecay(ParameterPoint &p) {
  using namespace AnyHdecay;
  static const Hdecay hdec{};
  auto result = hdec.r2hdm(static_cast<Yukawa>(p.type), TanBeta{p.tbeta},
                           SquaredMassPar{p.m12sq}, AMass{p.mA}, HcMass{p.mHp},
                           HMass{p.mHh}, HMass{p.mHl}, MixAngle{p.alpha});
  for (auto [key, value] : result)
    p.data.Store(std::string(key), value);
}

Interfaces::HiggsBoundsSignals::HBInput<R2HDM::nHzero, R2HDM::nHplus>
R2HDM::HiggsBoundsInput(
    ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {
  const std::array<double, 2> mH{p.mHl, p.mHh};
  Interfaces::HiggsBoundsSignals::HBInput<nHzero, nHplus> hb;
  for (size_t i = 0; i != nHzero - 1; ++i) {
    hb.Mh(i) = mH[i];
    hb.GammaTotal_hj(i) = p.data["w_"s + namesHzero[i]];
    hb.CP_value(i) = 1;
    // HiggsBounds_neutral_input_SMBR
    hb.BR_hjss(i) = p.data["b_"s + namesHzero[i] + "_ss"];
    hb.BR_hjcc(i) = p.data["b_"s + namesHzero[i] + "_cc"];
    hb.BR_hjbb(i) = p.data["b_"s + namesHzero[i] + "_bb"];
    hb.BR_hjtt(i) = p.data["b_"s + namesHzero[i] + "_tt"];
    hb.BR_hjmumu(i) = p.data["b_"s + namesHzero[i] + "_mumu"];
    hb.BR_hjtautau(i) = p.data["b_"s + namesHzero[i] + "_tautau"];
    hb.BR_hjWW(i) = p.data["b_"s + namesHzero[i] + "_WW"];
    hb.BR_hjZZ(i) = p.data["b_"s + namesHzero[i] + "_ZZ"];
    hb.BR_hjZga(i) = p.data["b_"s + namesHzero[i] + "_Zgam"];
    hb.BR_hjgaga(i) = p.data["b_"s + namesHzero[i] + "_gamgam"];
    hb.BR_hjgg(i) = p.data["b_"s + namesHzero[i] + "_gg"];
  }
  hb.Mh(2) = p.mA;
  hb.GammaTotal_hj(2) = p.data["w_A"];
  hb.CP_value(2) = -1;
  // HiggsBounds_neutral_input_SMBR
  hb.BR_hjss(2) = p.data["b_A_ss"];
  hb.BR_hjcc(2) = p.data["b_A_cc"];
  hb.BR_hjbb(2) = p.data["b_A_bb"];
  hb.BR_hjtt(2) = p.data["b_A_tt"];
  hb.BR_hjmumu(2) = p.data["b_A_mumu"];
  hb.BR_hjtautau(2) = p.data["b_A_tautau"];
  hb.BR_hjWW(2) = 0.;
  hb.BR_hjZZ(2) = 0.;
  hb.BR_hjZga(2) = p.data["b_A_Zgam"];
  hb.BR_hjgaga(2) = p.data["b_A_gamgam"];
  hb.BR_hjgg(2) = p.data["b_A_gg"];

  // HiggsBounds_neutral_input_nonSMBR
  hb.BR_hkhjhi(1, 0, 0) = p.data["b_Hh_HlHl"];
  hb.BR_hkhjhi(0, 2, 2) = p.data["b_Hl_AA"];
  hb.BR_hkhjhi(1, 2, 2) = p.data["b_Hh_AA"];
  hb.BR_hkhjhi(2, 1, 0) = hb.BR_hkhjhi(2, 0, 1);
  hb.BR_hjhiZ(0, 2) = p.data["b_Hl_ZA"];
  hb.BR_hjhiZ(1, 2) = p.data["b_Hh_ZA"];
  hb.BR_hjhiZ(2, 0) = p.data["b_A_ZHl"];
  hb.BR_hjhiZ(2, 1) = p.data["b_A_ZHh"];

  for (size_t i = 0; i != nHzero; ++i) {
    hb.BR_hjHpiW(i, 0) = p.data["b_"s + namesHzero[i] + "_WpHm"];
  }

  for (size_t i = 0; i != nHzero - 1; ++i) {
    // get the couplings
    const double cVV = p.data["c_"s + namesHzero[i] + "VV"];
    const double cu_e = p.data["c_"s + namesHzero[i] + "uu_e"];
    const double cd_e = p.data["c_"s + namesHzero[i] + "dd_e"];
    const double cl_e = p.data["c_"s + namesHzero[i] + "ll_e"];

    // HiggsBounds_neutral_input_LEP
    hb.XS_ee_hjZ_ratio(i) = pow(cVV, 2);
    hb.XS_ee_bbhj_ratio(i) = pow(cd_e, 2);
    hb.XS_ee_tautauhj_ratio(i) = pow(cl_e, 2);
    hb.XS_ee_hjhi_ratio(i, 2) = pow(p.data["c_"s + namesHzero[i] + "AZ"], 2);
    hb.XS_ee_hjhi_ratio(2, i) = hb.XS_ee_hjhi_ratio(i, 2);

    // HiggsBounds_neutral_input_hadr
    //   Tevatron
    double gg =
        cxnH0_.GGe(mH[i], cu_e, cd_e, Tools::SushiTables::Collider::TEV);
    double bb = cxnH0_.BBe(mH[i], cd_e, Tools::SushiTables::Collider::TEV);
    double gg0 = cxnH0_.GGe(mH[i], 1, 1, Tools::SushiTables::Collider::TEV);
    double bb0 = cxnH0_.BBe(mH[i], 1, Tools::SushiTables::Collider::TEV);
    hb.TEV_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.TEV_CS_gg_hj_ratio(i) = gg / gg0;
    hb.TEV_CS_bb_hj_ratio(i) = bb / bb0;
    hb.TEV_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_tthj_ratio(i) = pow(cu_e, 2);      //< neglects ZH>ttH diagrams
    hb.TEV_CS_thj_tchan_ratio(i) = pow(cu_e, 2); //< LO correct
    hb.TEV_CS_thj_schan_ratio(i) = pow(cu_e, 2); //< LO correct
    //   LHC7
    gg = cxnH0_.GGe(mH[i], cu_e, cd_e, Tools::SushiTables::Collider::LHC7);
    bb = cxnH0_.BBe(mH[i], cd_e, Tools::SushiTables::Collider::LHC7);
    gg0 = cxnH0_.GGe(mH[i], 1, 1, Tools::SushiTables::Collider::LHC7);
    bb0 = cxnH0_.BBe(mH[i], 1, Tools::SushiTables::Collider::LHC7);
    hb.LHC7_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC7_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC7_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC7_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_tthj_ratio(i) = pow(cu_e, 2);      //< neglects ZH>ttH diagrams
    hb.LHC7_CS_thj_tchan_ratio(i) = pow(cu_e, 2); //< LO correct
    hb.LHC7_CS_thj_schan_ratio(i) = pow(cu_e, 2); //< LO correct
    //   LHC8
    gg = cxnH0_.GGe(mH[i], cu_e, cd_e, Tools::SushiTables::Collider::LHC8);
    bb = cxnH0_.BBe(mH[i], cd_e, Tools::SushiTables::Collider::LHC8);
    gg0 = cxnH0_.GGe(mH[i], 1, 1, Tools::SushiTables::Collider::LHC8);
    bb0 = cxnH0_.BBe(mH[i], 1, Tools::SushiTables::Collider::LHC8);
    hb.LHC8_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC8_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC8_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC8_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_tthj_ratio(i) = pow(cu_e, 2);      //< neglects ZH>ttH diagrams
    hb.LHC8_CS_thj_tchan_ratio(i) = pow(cu_e, 2); //< LO correct
    hb.LHC8_CS_thj_schan_ratio(i) = pow(cu_e, 2); //< LO correct
    //   LHC13
    gg = cxnH0_.GGe(mH[i], cu_e, cd_e, Tools::SushiTables::Collider::LHC13);
    bb = cxnH0_.BBe(mH[i], cd_e, Tools::SushiTables::Collider::LHC13);
    gg0 = cxnH0_.GGe(mH[i], 1, 1, Tools::SushiTables::Collider::LHC13);
    bb0 = cxnH0_.BBe(mH[i], 1, Tools::SushiTables::Collider::LHC13);
    hb.LHC13_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC13_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC13_CS_bb_hj_ratio(i) = bb / bb0;
    const auto x_VH = hbhs.GetVHCxns(mH[i], cVV, cu_e, cd_e);
    const auto x_VH_ref = hbhs.GetVHCxns(mH[i], 1, 1, 1);
    hb.LHC13_CS_hjZ_ratio(i) = x_VH.x_hZ / x_VH_ref.x_hZ;
    hb.LHC13_CS_gg_hjZ_ratio(i) = x_VH.x_gg_hZ / x_VH_ref.x_gg_hZ;
    hb.LHC13_CS_qq_hjZ_ratio(i) = x_VH.x_qq_hZ / x_VH_ref.x_qq_hZ;
    hb.LHC13_CS_hjW_ratio(i) = x_VH.x_hW / x_VH_ref.x_hW;
    hb.LHC13_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC13_CS_tthj_ratio(i) = pow(cu_e, 2);      //< neglects ZH>ttH diagrams
    hb.LHC13_CS_thj_tchan_ratio(i) = pow(cu_e, 2); //< LO correct
    hb.LHC13_CS_thj_schan_ratio(i) = pow(cu_e, 2); //< LO correct
    hb.LHC13_CS_tWhj_ratio(i) =
        Interfaces::HiggsBoundsSignals::tWHratio(cVV, cu_e);
  }

  // get the couplings
  const double cu_o = p.data["c_Auu_o"];
  const double cd_o = p.data["c_Add_o"];
  const double cl_o = p.data["c_All_o"];

  // HiggsBounds_neutral_input_LEP
  hb.XS_ee_hjZ_ratio(2) = 0.;
  hb.XS_ee_bbhj_ratio(2) = pow(cd_o, 2);
  hb.XS_ee_tautauhj_ratio(2) = pow(cl_o, 2);
  // hb.XS_ee_hjhi_ratio(i, 2) and hb.XS_ee_hjhi_ratio(2, i) already filled
  // above

  // HiggsBounds_neutral_input_hadr
  //   Tevatron
  double gg = cxnH0_.GGo(p.mA, cu_o, cd_o, Tools::SushiTables::Collider::TEV);
  double bb = cxnH0_.BBo(p.mA, cd_o, Tools::SushiTables::Collider::TEV);
  double gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::TEV);
  double bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::TEV);
  hb.TEV_CS_hj_ratio(2) = (gg + bb) / (gg0 + bb0);
  hb.TEV_CS_gg_hj_ratio(2) = gg / gg0;
  hb.TEV_CS_bb_hj_ratio(2) = bb / bb0;
  hb.TEV_CS_hjW_ratio(2) = 0.;
  hb.TEV_CS_hjZ_ratio(2) = 0.;
  hb.TEV_CS_vbf_ratio(2) = 0.;
  hb.TEV_CS_tthj_ratio(2) = pow(cu_o, 2);      //< neglects ZH>ttH diagrams
  hb.TEV_CS_thj_tchan_ratio(2) = pow(cu_o, 2); //< LO correct
  hb.TEV_CS_thj_schan_ratio(2) = pow(cu_o, 2); //< LO correct
  //   LHC7
  gg = cxnH0_.GGo(p.mA, cu_o, cd_o, Tools::SushiTables::Collider::LHC7);
  bb = cxnH0_.BBo(p.mA, cd_o, Tools::SushiTables::Collider::LHC7);
  gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::LHC7);
  bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::LHC7);
  hb.LHC7_CS_hj_ratio(2) = (gg + bb) / (gg0 + bb0);
  hb.LHC7_CS_gg_hj_ratio(2) = gg / gg0;
  hb.LHC7_CS_bb_hj_ratio(2) = bb / bb0;
  hb.LHC7_CS_hjW_ratio(2) = 0.;
  hb.LHC7_CS_hjZ_ratio(2) = 0.;
  hb.LHC7_CS_vbf_ratio(2) = 0.;
  hb.LHC7_CS_tthj_ratio(2) = pow(cu_o, 2);      //< neglects ZH>ttH diagrams
  hb.LHC7_CS_thj_tchan_ratio(2) = pow(cu_o, 2); //< LO correct
  hb.LHC7_CS_thj_schan_ratio(2) = pow(cu_o, 2); //< LO correct
  //   LHC8
  gg = cxnH0_.GGo(p.mA, cu_o, cd_o, Tools::SushiTables::Collider::LHC8);
  bb = cxnH0_.BBo(p.mA, cd_o, Tools::SushiTables::Collider::LHC8);
  gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::LHC8);
  bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::LHC8);
  hb.LHC8_CS_hj_ratio(2) = (gg + bb) / (gg0 + bb0);
  hb.LHC8_CS_gg_hj_ratio(2) = gg / gg0;
  hb.LHC8_CS_bb_hj_ratio(2) = bb / bb0;
  hb.LHC8_CS_hjW_ratio(2) = 0.;
  hb.LHC8_CS_hjZ_ratio(2) = 0.;
  hb.LHC8_CS_vbf_ratio(2) = 0.;
  hb.LHC8_CS_tthj_ratio(2) = pow(cu_o, 2);      //< neglects ZH>ttH diagrams
  hb.LHC8_CS_thj_tchan_ratio(2) = pow(cu_o, 2); //< LO correct
  hb.LHC8_CS_thj_schan_ratio(2) = pow(cu_o, 2); //< LO correct
  //   LHC13
  gg = cxnH0_.GGo(p.mA, cu_o, cd_o, Tools::SushiTables::Collider::LHC13);
  bb = cxnH0_.BBo(p.mA, cd_o, Tools::SushiTables::Collider::LHC13);
  gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::LHC13);
  bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::LHC13);
  hb.LHC13_CS_hj_ratio(2) = (gg + bb) / (gg0 + bb0);
  hb.LHC13_CS_gg_hj_ratio(2) = gg / gg0;
  hb.LHC13_CS_bb_hj_ratio(2) = bb / bb0;
  const auto x_VH = hbhs.GetVHCxns(p.mA, 0, {0, cu_o}, {0, cd_o});
  const auto x_VH_ref = hbhs.GetVHCxns(p.mA, 1, 1, 1);
  hb.LHC13_CS_hjZ_ratio(2) = x_VH.x_hZ / x_VH_ref.x_hZ;
  hb.LHC13_CS_gg_hjZ_ratio(2) = x_VH.x_gg_hZ / x_VH_ref.x_gg_hZ;
  hb.LHC13_CS_qq_hjZ_ratio(2) = x_VH.x_qq_hZ / x_VH_ref.x_qq_hZ;
  hb.LHC13_CS_hjW_ratio(2) = 0;
  hb.LHC13_CS_vbf_ratio(2) = 0.;
  hb.LHC13_CS_tthj_ratio(2) = pow(cu_o, 2);      //< neglects ZH>ttH diagrams
  hb.LHC13_CS_thj_tchan_ratio(2) = pow(cu_o, 2); //< LO correct
  hb.LHC13_CS_thj_schan_ratio(2) = pow(cu_o, 2); //< LO correct
  hb.LHC13_CS_tWhj_ratio(2) =
      Interfaces::HiggsBoundsSignals::tWHratio(0., {0, cu_o});

  hb.Mhplus(0) = p.mHp;
  hb.GammaTotal_Hpj(0) = p.data["w_Hp"];
  hb.CS_ee_HpjHmj_ratio(0) = 1;
  hb.BR_tWpb = p.data["b_t_Wb"];
  hb.BR_tHpjb(0) = p.data["b_t_Hpb"];
  hb.BR_Hpjcs(0) = p.data["b_Hp_cs"];
  hb.BR_Hpjcb(0) = p.data["b_Hp_cb"];
  hb.BR_Hpjtaunu(0) = p.data["b_Hp_taunu"];
  hb.BR_Hpjtb(0) = p.data["b_Hp_tb"];
  // BR_HpjWZ(0) == 0
  for (size_t i = 0; i != nHzero; ++i)
    hb.BR_HpjhiW(0, i) = p.data["b_Hp_W"s + namesHzero[i]];

  // charged Higgs cxn (as of Oct 2018 the only one needed)
  auto coupsHp = TwoHDMHpCoups(p.tbeta, p.type);
  p.data.Store("x_tHpm", hbhs.GetHpCxn(p.mHp, coupsHp.rhot, coupsHp.rhob,
                                       p.data["b_t_Hpb"]));
  hb.LHC13_CS_Hpjtb(0) = p.data["x_tHpm"];
  return hb;
}

Constraints::STUDetail::STUParameters R2HDM::STUInput(const ParameterPoint &p) {
  Constraints::STUDetail::STUParameters res{Eigen::MatrixXcd(2, nHzero + 1),
                                            Eigen::Matrix2d(),
                                            {p.mHh, p.mHl, p.mA},
                                            {p.mHp}};
  const double cb = cos(atan(p.tbeta));
  const double sb = sin(atan(p.tbeta));
  const double ca = cos(p.alpha);
  const double sa = sin(p.alpha);

  res.mU << cb, -sb, sb, cb;

  res.mV(0, 0) = {0, cb};
  res.mV(0, 1) = {ca, 0};
  res.mV(0, 2) = {-sa, 0};
  res.mV(0, 3) = {0, -sb};

  res.mV(1, 0) = {0, sb};
  res.mV(1, 1) = {sa, 0};
  res.mV(1, 2) = {ca, 0};
  res.mV(1, 3) = {0, cb};

  return res;
}

bool R2HDM::EWPValid(const ParameterPoint &p) {
  return (p.mHp + p.mHl > Constants::mW) && (p.mHp + p.mHh > Constants::mW) &&
         (p.mHl + p.mA > Constants::mZ) && (p.mHh + p.mA > Constants::mZ);
}

// ---------------------------------- Theory ----------------------------------
bool R2HDM::BFB(const std::array<double, 5> &L) {
  return TwoHDM::BFB(L[0], L[1], L[2], L[3], std::abs(L[4]));
}

double R2HDM::MaxUnitarityEV(const std::array<double, 5> &L) {
  return TwoHDM::MaxUnitarityEV(L[0], L[1], L[2], L[3], std::abs(L[4]));
}

bool R2HDM::AbsoluteStability(const ParameterPoint &p) {
  using std::pow;
  using std::sqrt;
  const double k = pow(p.L[0] / p.L[1], 1 / 4.);
  const double disc = p.m12sq * (p.m11sq - k * k * p.m22sq) * (p.tbeta - k);
  return disc > 0;
}

const Tools::SushiTables R2HDM::cxnH0_{};

std::string R2HDM::ParameterPoint::ToString() const {
  std::ostringstream os;
  auto printer = Utilities::TSVPrinter(os);
  printer << mHl << mHh << mA << mHp;
  printer << tbeta << m12sq << alpha;
  for (double l : L)
    printer << l;
  printer << m11sq << m22sq;
  printer << static_cast<int>(type);
  printer << v;
  for (const auto &[key, value] : data)
    printer << value;
  return os.str();
}

void R2HDM::CalcCouplings(ParameterPoint &p) {
  using std::cos;
  using std::sin;
  const double sb = sin(std::atan(p.tbeta));
  const double cb = cos(std::atan(p.tbeta));
  const double sa = sin(p.alpha);
  const double ca = cos(p.alpha);
  const double sbma = sin(std::atan(p.tbeta) - p.alpha);
  const double cbma = cos(std::atan(p.tbeta) - p.alpha);

  // gauge couplings
  p.data.Store("c_HhVV", cbma);
  p.data.Store("c_HlVV", sbma);

  p.data.Store("c_HhAZ", sbma);
  p.data.Store("c_HlAZ", cbma);

  // fermion couplings
  p.data.Store("c_Hluu_e", ca / sb);
  p.data.Store("c_Hhuu_e", sa / sb);
  p.data.Store("c_Auu_o", 1 / p.tbeta);
  if (p.type == Yuk::typeI) {
    p.data.Store("c_Hldd_e", ca / sb);
    p.data.Store("c_Hhdd_e", sa / sb);
    p.data.Store("c_Add_o", -1 / p.tbeta);
    p.data.Store("c_Hlll_e", ca / sb);
    p.data.Store("c_Hhll_e", sa / sb);
    p.data.Store("c_All_o", -1 / p.tbeta);
  }
  if (p.type == Yuk::typeII) {
    p.data.Store("c_Hldd_e", -sa / cb);
    p.data.Store("c_Hhdd_e", ca / cb);
    p.data.Store("c_Add_o", p.tbeta);
    p.data.Store("c_Hlll_e", -sa / cb);
    p.data.Store("c_Hhll_e", ca / cb);
    p.data.Store("c_All_o", p.tbeta);
  }
  if (p.type == Yuk::leptonSpecific) { // Lepton Specific
    p.data.Store("c_Hldd_e", ca / sb);
    p.data.Store("c_Hhdd_e", sa / sb);
    p.data.Store("c_Add_o", -1 / p.tbeta);
    p.data.Store("c_Hlll_e", -sa / cb);
    p.data.Store("c_Hhll_e", ca / cb);
    p.data.Store("c_All_o", p.tbeta);
  }
  if (p.type == Yuk::flipped) { // flipped
    p.data.Store("c_Hldd_e", -sa / cb);
    p.data.Store("c_Hhdd_e", ca / cb);
    p.data.Store("c_Add_o", p.tbeta);
    p.data.Store("c_Hlll_e", ca / sb);
    p.data.Store("c_Hhll_e", sa / sb);
    p.data.Store("c_All_o", -1 / p.tbeta);
  }

  // charged H+ H- Hi couplings in the sign convention of 1507.00933 (i.e.
  // relative sign to WW means destructive interference)
  p.data.Store(
      "c_HlHpHm",
      (p.tbeta * p.v * (-p.L[1] - pow(p.tbeta, 2) * p.L[2] + p.L[3] + p.L[4]) *
           cos(p.alpha) +
       p.v * (p.L[2] + pow(p.tbeta, 2) * (p.L[0] - p.L[3] - p.L[4])) *
           sin(p.alpha)) /
          pow(1 + pow(p.tbeta, 2), 1.5));

  p.data.Store(
      "c_HhHpHm",
      (p.v * (-p.L[2] + pow(p.tbeta, 2) * (-p.L[0] + p.L[3] + p.L[4])) *
           cos(p.alpha) +
       p.tbeta * p.v * (-p.L[1] - pow(p.tbeta, 2) * p.L[2] + p.L[3] + p.L[4]) *
           sin(p.alpha)) /
          pow(1 + pow(p.tbeta, 2), 1.5));
}

void R2HDM::CalcCXNs(ParameterPoint &p) {
  const std::array<double, 2> mH{p.mHl, p.mHh};
  for (size_t i = 0; i != nHzero - 1; ++i) {
    p.data.Store("x_"s + namesHzero[i] + "_ggH",
                 cxnH0_.GGe(mH[i], p.data["c_"s + namesHzero[i] + "uu_e"],
                            p.data["c_"s + namesHzero[i] + "dd_e"],
                            Tools::SushiTables::Collider::LHC13));
    p.data.Store("x_"s + namesHzero[i] + "_bbH",
                 cxnH0_.BBe(mH[i], p.data["c_"s + namesHzero[i] + "dd_e"],
                            Tools::SushiTables::Collider::LHC13));
  }
  p.data.Store("x_A_ggH", cxnH0_.GGo(p.mA, p.data["c_Auu_o"], p.data["c_Add_o"],
                                     Tools::SushiTables::Collider::LHC13));
  p.data.Store("x_A_bbH", cxnH0_.BBo(p.mA, p.data["c_Add_o"],
                                     Tools::SushiTables::Collider::LHC13));
}

std::vector<double> R2HDM::BsmptInput(const ParameterPoint &p) {
  return {p.L[0], p.L[1],  p.L[2],  p.L[3],
          p.L[4], p.m12sq, p.tbeta, static_cast<double>(p.type)};
}

} // namespace ScannerS::Models
