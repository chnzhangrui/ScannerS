#include "ScannerS/Models/N2HDMBroken.hpp"

#include "AnyHdecay.hpp"
#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Interfaces/HiggsBoundsSignals.hpp"
#include "ScannerS/Tools/SushiTables.hpp"
#include "ScannerS/Utilities.hpp"
#include <cmath>
#include <complex>
#include <map>
#include <sstream>
#include <utility>

namespace ScannerS::Models {

// -------------------------------- Generation --------------------------------
namespace {
Eigen::Matrix3d PhysicalMixMat(const N2HDMBroken::PhysicalInput &in) {
  double c_H1VV = sqrt(in.c_HaVV_sq);
  double c_H1tt = sqrt(in.c_Hatt_sq);
  const double cb = cos(atan(in.tbeta));
  const double sb = sin(atan(in.tbeta));
  double R11 = c_H1VV / cb - c_H1tt * sb * in.tbeta;

  if (R11 < 0) { // make positive by flipping signs of VV and tt coupling
    R11 *= -1;
    c_H1tt *= -1;
    // c_H1VV *= -1; // not needed
  }
  const double R12 = c_H1tt * sb;
  if (R11 * R11 + R12 * R12 > 1) {
    return Eigen::Matrix3d::Constant(2); // invalid
  }
  double R13 = in.sign_Ra3 * sqrt(1 - R11 * R11 - R12 * R12);
  if (R13 * R13 + in.Rb3 * in.Rb3 > 1) {
    return Eigen::Matrix3d::Constant(2);
  }
  const double a2 = asin(R13);
  const double a1 = atan(R12 / R11);
  const double a3 = asin(in.Rb3 / cos(a2));

  return Utilities::OrderedMixMat3d(a1, a2, a3, {in.mHa, in.mHb, in.mHc});
}

std::array<double, 8> CalcLambdas(const std::array<double, 3> &mHi, double mA,
                                  double mHp, double tbeta,
                                  const Eigen::Matrix3d &R, double vs, double v,
                                  double m12sq) {
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  const double mu = m12sq / sb / cb;
  using std::pow;

  const double L1 =
      (-(mu * pow(sb, 2)) + pow(mHi[0], 2) * pow(R(0, 0), 2) +
       pow(mHi[1], 2) * pow(R(1, 0), 2) + pow(mHi[2], 2) * pow(R(2, 0), 2)) /
      (pow(cb, 2) * pow(v, 2));
  const double L2 =
      (-(pow(cb, 2) * mu) + pow(mHi[0], 2) * pow(R(0, 1), 2) +
       pow(mHi[1], 2) * pow(R(1, 1), 2) + pow(mHi[2], 2) * pow(R(2, 1), 2)) /
      (pow(sb, 2) * pow(v, 2));
  const double L3 = (-mu + 2 * pow(mHp, 2) +
                     (pow(mHi[0], 2) * R(0, 0) * R(0, 1) +
                      pow(mHi[1], 2) * R(1, 0) * R(1, 1) +
                      pow(mHi[2], 2) * R(2, 0) * R(2, 1)) /
                         (cb * sb)) /
                    pow(v, 2);
  const double L4 = (mu + pow(mA, 2) - 2 * pow(mHp, 2)) / pow(v, 2);
  const double L5 = (mu - pow(mA, 2)) / pow(v, 2);
  const double L6 =
      (pow(mHi[0], 2) * pow(R(0, 2), 2) + pow(mHi[1], 2) * pow(R(1, 2), 2) +
       pow(mHi[2], 2) * pow(R(2, 2), 2)) /
      pow(vs, 2);
  const double L7 =
      (pow(mHi[0], 2) * R(0, 0) * R(0, 2) + pow(mHi[1], 2) * R(1, 0) * R(1, 2) +
       pow(mHi[2], 2) * R(2, 0) * R(2, 2)) /
      (cb * v * vs);
  const double L8 =
      (pow(mHi[0], 2) * R(0, 1) * R(0, 2) + pow(mHi[1], 2) * R(1, 1) * R(1, 2) +
       pow(mHi[2], 2) * R(2, 1) * R(2, 2)) /
      (sb * v * vs);
  return {L1, L2, L3, L4, L5, L6, L7, L8};
}

double CalcM11sq(double tbeta, double vs, double v, double m12sq,
                 const std::array<double, 8> &L) {
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  return m12sq * tbeta - (pow(cb, 2) * pow(v, 2) * L[0]) / 2. -
         (pow(sb, 2) * pow(v, 2) * L[2]) / 2. -
         (pow(sb, 2) * pow(v, 2) * L[3]) / 2. -
         (pow(sb, 2) * pow(v, 2) * L[4]) / 2. - (pow(vs, 2) * L[6]) / 2.;
}

double CalcM22sq(double tbeta, double vs, double v, double m12sq,
                 const std::array<double, 8> &L) {
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  return m12sq / tbeta - (pow(sb, 2) * pow(v, 2) * L[1]) / 2. -
         (pow(cb, 2) * pow(v, 2) * L[2]) / 2. -
         (pow(cb, 2) * pow(v, 2) * L[3]) / 2. -
         (pow(cb, 2) * pow(v, 2) * L[4]) / 2. - (pow(vs, 2) * L[7]) / 2.;
}

double CalcMssq(double tbeta, double vs, double v,
                const std::array<double, 8> &L) {
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  return -(pow(vs, 2) * L[5]) / 2. - (pow(cb, 2) * pow(v, 2) * L[6]) / 2. -
         (pow(sb, 2) * pow(v, 2) * L[7]) / 2.;
}
} // namespace

N2HDMBroken::ParameterPoint::ParameterPoint(const AngleInput &in)
    : mHi{Utilities::Sorted(std::array<double, 3>({in.mHa, in.mHb, in.mHc}))},
      mA{in.mA}, mHp{in.mHp}, tbeta{in.tbeta}, R{Utilities::OrderedMixMat3d(
                                                   in.a1, in.a2, in.a3,
                                                   {in.mHa, in.mHb, in.mHc})},
      alpha{Utilities::MixMatAngles3d(R)}, type{in.type}, vs{in.vs}, v{in.v},
      m12sq{in.m12sq}, L{CalcLambdas(mHi, mA, mHp, tbeta, R, vs, v, m12sq)},
      m11sq{CalcM11sq(tbeta, vs, v, m12sq, L)},
      m22sq{CalcM22sq(tbeta, vs, v, m12sq, L)}, mssq{CalcMssq(tbeta, vs, v,
                                                              L)} {}

N2HDMBroken::ParameterPoint::ParameterPoint(const PhysicalInput &in)
    : mHi{Utilities::Sorted(std::array<double, 3>({in.mHa, in.mHb, in.mHc}))},
      mA{in.mA}, mHp{in.mHp}, tbeta{in.tbeta}, R{PhysicalMixMat(in)},
      alpha{Utilities::MixMatAngles3d(R)}, type{in.type}, vs{in.vs}, v{in.v},
      m12sq{in.m12sq}, L{CalcLambdas(mHi, mA, mHp, tbeta, R, vs, v, m12sq)},
      m11sq{CalcM11sq(tbeta, vs, v, m12sq, L)},
      m22sq{CalcM22sq(tbeta, vs, v, m12sq, L)}, mssq{CalcMssq(tbeta, vs, v,
                                                              L)} {}

// ----------------------------------- Pheno ----------------------------------
void N2HDMBroken::RunHdecay(ParameterPoint &p) {
  using namespace AnyHdecay;
  static const Hdecay hdec{};
  auto result = hdec.n2hdmBroken(
      static_cast<Yukawa>(p.type), TanBeta{p.tbeta}, SquaredMassPar{p.m12sq},
      AMass{p.mA}, HcMass{p.mHp}, HMass{p.mHi[0]}, HMass{p.mHi[1]},
      HMass{p.mHi[2]}, MixAngle{p.alpha[0]}, MixAngle{p.alpha[1]},
      MixAngle{p.alpha[2]}, SingletVev{p.vs});
  for (auto [key, value] : result)
    p.data.Store(std::string(key), value);
}

Interfaces::HiggsBoundsSignals::HBInput<N2HDM::nHzero, N2HDM::nHplus>
N2HDMBroken::HiggsBoundsInput(
    ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {

  Interfaces::HiggsBoundsSignals::HBInput<nHzero, nHplus> hb;
  for (size_t I = 1; I != nHzero; ++I) {
    hb.Mh(I) = p.mHi[I - 1];
    hb.CP_value(I) = 1;
  }
  hb.Mh(0) = p.mA;
  hb.CP_value(0) = -1;

  for (size_t i = 0; i != nHzero; ++i) {
    hb.GammaTotal_hj(i) = p.data["w_"s + namesHzero[i]];
    // HiggsBounds_neutral_input_SMBR
    hb.BR_hjss(i) = p.data["b_"s + namesHzero[i] + "_ss"];
    hb.BR_hjcc(i) = p.data["b_"s + namesHzero[i] + "_cc"];
    hb.BR_hjbb(i) = p.data["b_"s + namesHzero[i] + "_bb"];
    hb.BR_hjtt(i) = p.data["b_"s + namesHzero[i] + "_tt"];
    hb.BR_hjmumu(i) = p.data["b_"s + namesHzero[i] + "_mumu"];
    hb.BR_hjtautau(i) = p.data["b_"s + namesHzero[i] + "_tautau"];
    hb.BR_hjZga(i) = p.data["b_"s + namesHzero[i] + "_Zgam"];
    hb.BR_hjgaga(i) = p.data["b_"s + namesHzero[i] + "_gamgam"];
    hb.BR_hjgg(i) = p.data["b_"s + namesHzero[i] + "_gg"];
  }
  for (size_t I = 1; I != nHzero; ++I) {
    hb.BR_hjWW(I) = p.data["b_"s + namesHzero[I] + "_WW"];
    hb.BR_hjZZ(I) = p.data["b_"s + namesHzero[I] + "_ZZ"];
  }
  // HiggsBounds_neutral_input_nonSMBR=
  hb.BR_hkhjhi(2, 1, 1) = p.data["b_H2_H1H1"];
  hb.BR_hkhjhi(3, 1, 1) = p.data["b_H3_H1H1"];
  hb.BR_hkhjhi(3, 2, 2) = p.data["b_H3_H2H2"];
  hb.BR_hkhjhi(3, 1, 2) = p.data["b_H3_H1H2"];
  hb.BR_hkhjhi(3, 2, 1) = hb.BR_hkhjhi(3, 1, 2);
  for (size_t I = 1; I != nHzero; ++I) {
    hb.BR_hkhjhi(I, 0, 0) = p.data["b_"s + namesHzero[I] + "_AA"];
    hb.BR_hjhiZ(I, 0) = p.data["b_"s + namesHzero[I] + "_ZA"];
    hb.BR_hjhiZ(0, I) = p.data["b_A_Z"s + namesHzero[I]];
  }
  for (size_t i = 0; i != nHzero; ++i) {
    hb.BR_hjHpiW(i, 0) = p.data["b_"s + namesHzero[i] + "_WpHm"];
  }

  for (size_t I = 1; I != nHzero; ++I) {
    const double cVV = p.data["c_"s + namesHzero[I] + "VV"];
    const double cu = p.data["c_"s + namesHzero[I] + "uu_e"];
    const double cd = p.data["c_"s + namesHzero[I] + "dd_e"];
    const double cl = p.data["c_"s + namesHzero[I] + "ll_e"];
    // HiggsBounds_neutral_input_LEP
    hb.XS_ee_hjZ_ratio(I) = pow(cVV, 2);
    hb.XS_ee_bbhj_ratio(I) = pow(cd, 2);
    hb.XS_ee_tautauhj_ratio(I) = pow(cl, 2);
    hb.XS_ee_hjhi_ratio(I, 0) = pow(p.data["c_"s + namesHzero[I] + "AZ"], 2);
    hb.XS_ee_hjhi_ratio(0, I) = hb.XS_ee_hjhi_ratio(I, 0);
    // HiggsBounds_neutral_input_hadr
    //   Tevatron
    double gg =
        cxnH0_.GGe(p.mHi[I - 1], cu, cd, Tools::SushiTables::Collider::TEV);
    double bb = cxnH0_.BBe(p.mHi[I - 1], cd, Tools::SushiTables::Collider::TEV);
    double gg0 =
        cxnH0_.GGe(p.mHi[I - 1], 1, 1, Tools::SushiTables::Collider::TEV);
    double bb0 = cxnH0_.BBe(p.mHi[I - 1], 1, Tools::SushiTables::Collider::TEV);
    hb.TEV_CS_hj_ratio(I) = (gg + bb) / (gg0 + bb0);
    hb.TEV_CS_gg_hj_ratio(I) = gg / gg0;
    hb.TEV_CS_bb_hj_ratio(I) = bb / bb0;
    hb.TEV_CS_hjW_ratio(I) = pow(cVV, 2);
    hb.TEV_CS_hjZ_ratio(I) = pow(cVV, 2);
    hb.TEV_CS_vbf_ratio(I) = pow(cVV, 2);
    hb.TEV_CS_tthj_ratio(I) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.TEV_CS_thj_tchan_ratio(I) = pow(cu, 2); //< LO correct
    hb.TEV_CS_thj_schan_ratio(I) = pow(cu, 2); //< LO correct
    //   LHC7
    gg = cxnH0_.GGe(p.mHi[I - 1], cu, cd, Tools::SushiTables::Collider::LHC7);
    bb = cxnH0_.BBe(p.mHi[I - 1], cd, Tools::SushiTables::Collider::LHC7);
    gg0 = cxnH0_.GGe(p.mHi[I - 1], 1, 1, Tools::SushiTables::Collider::LHC7);
    bb0 = cxnH0_.BBe(p.mHi[I - 1], 1, Tools::SushiTables::Collider::LHC7);
    hb.LHC7_CS_hj_ratio(I) = (gg + bb) / (gg0 + bb0);
    hb.LHC7_CS_gg_hj_ratio(I) = gg / gg0;
    hb.LHC7_CS_bb_hj_ratio(I) = bb / bb0;
    hb.LHC7_CS_hjW_ratio(I) = pow(cVV, 2);
    hb.LHC7_CS_hjZ_ratio(I) = pow(cVV, 2);
    hb.LHC7_CS_vbf_ratio(I) = pow(cVV, 2);
    hb.LHC7_CS_tthj_ratio(I) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.LHC7_CS_thj_tchan_ratio(I) = pow(cu, 2); //< LO correct
    hb.LHC7_CS_thj_schan_ratio(I) = pow(cu, 2); //< LO correct
    //   LHC8
    gg = cxnH0_.GGe(p.mHi[I - 1], cu, cd, Tools::SushiTables::Collider::LHC8);
    bb = cxnH0_.BBe(p.mHi[I - 1], cd, Tools::SushiTables::Collider::LHC8);
    gg0 = cxnH0_.GGe(p.mHi[I - 1], 1, 1, Tools::SushiTables::Collider::LHC8);
    bb0 = cxnH0_.BBe(p.mHi[I - 1], 1, Tools::SushiTables::Collider::LHC8);
    hb.LHC8_CS_hj_ratio(I) = (gg + bb) / (gg0 + bb0);
    hb.LHC8_CS_gg_hj_ratio(I) = gg / gg0;
    hb.LHC8_CS_bb_hj_ratio(I) = bb / bb0;
    hb.LHC8_CS_hjW_ratio(I) = pow(cVV, 2);
    hb.LHC8_CS_hjZ_ratio(I) = pow(cVV, 2);
    hb.LHC8_CS_vbf_ratio(I) = pow(cVV, 2);
    hb.LHC8_CS_tthj_ratio(I) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.LHC8_CS_thj_tchan_ratio(I) = pow(cu, 2); //< LO correct
    hb.LHC8_CS_thj_schan_ratio(I) = pow(cu, 2); //< LO correct
    //   LHC13
    gg = cxnH0_.GGe(p.mHi[I - 1], cu, cd, Tools::SushiTables::Collider::LHC13);
    bb = cxnH0_.BBe(p.mHi[I - 1], cd, Tools::SushiTables::Collider::LHC13);
    gg0 = cxnH0_.GGe(p.mHi[I - 1], 1, 1, Tools::SushiTables::Collider::LHC13);
    bb0 = cxnH0_.BBe(p.mHi[I - 1], 1, Tools::SushiTables::Collider::LHC13);
    hb.LHC13_CS_hj_ratio(I) = (gg + bb) / (gg0 + bb0);
    hb.LHC13_CS_gg_hj_ratio(I) = gg / gg0;
    hb.LHC13_CS_bb_hj_ratio(I) = bb / bb0;
    const auto x_VH = hbhs.GetVHCxns(p.mHi[I - 1], cVV, cu, cd);
    const auto x_VH_ref = hbhs.GetVHCxns(p.mHi[I - 1], 1, 1, 1);
    hb.LHC13_CS_hjZ_ratio(I) = x_VH.x_hZ / x_VH_ref.x_hZ;
    hb.LHC13_CS_gg_hjZ_ratio(I) = x_VH.x_gg_hZ / x_VH_ref.x_gg_hZ;
    hb.LHC13_CS_qq_hjZ_ratio(I) = x_VH.x_qq_hZ / x_VH_ref.x_qq_hZ;
    hb.LHC13_CS_hjW_ratio(I) = x_VH.x_hW / x_VH_ref.x_hW;
    hb.LHC13_CS_vbf_ratio(I) = pow(cVV, 2);
    hb.LHC13_CS_tthj_ratio(I) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.LHC13_CS_thj_tchan_ratio(I) = pow(cu, 2); //< LO correct
    hb.LHC13_CS_thj_schan_ratio(I) = pow(cu, 2); //< LO correct
    hb.LHC13_CS_tWhj_ratio(I) =
        Interfaces::HiggsBoundsSignals::tWHratio(cVV, cu);
  }

  // --- same for A ---
  const double cu = p.data["c_Auu_o"];
  const double cd = p.data["c_Add_o"];
  const double cl = p.data["c_All_o"];
  // HiggsBounds_neutral_input_LEP
  hb.XS_ee_bbhj_ratio(0) = pow(cd, 2);
  hb.XS_ee_tautauhj_ratio(0) = pow(cl, 2);
  // HiggsBounds_neutral_input_hadr
  //   Tevatron
  double gg = cxnH0_.GGo(p.mA, cu, cd, Tools::SushiTables::Collider::TEV);
  double bb = cxnH0_.BBo(p.mA, cd, Tools::SushiTables::Collider::TEV);
  double gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::TEV);
  double bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::TEV);
  hb.TEV_CS_hj_ratio(0) = (gg + bb) / (gg0 + bb0);
  hb.TEV_CS_gg_hj_ratio(0) = gg / gg0;
  hb.TEV_CS_bb_hj_ratio(0) = bb / bb0;
  hb.TEV_CS_tthj_ratio(0) = pow(cu, 2);      //< neglects ZH>ttH diagrams
  hb.TEV_CS_thj_tchan_ratio(0) = pow(cu, 2); //< LO correct
  hb.TEV_CS_thj_schan_ratio(0) = pow(cu, 2); //< LO correct
  //   LHC7
  gg = cxnH0_.GGo(p.mA, cu, cd, Tools::SushiTables::Collider::LHC7);
  bb = cxnH0_.BBo(p.mA, cd, Tools::SushiTables::Collider::LHC7);
  gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::LHC7);
  bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::LHC7);
  hb.LHC7_CS_hj_ratio(0) = (gg + bb) / (gg0 + bb0);
  hb.LHC7_CS_gg_hj_ratio(0) = gg / gg0;
  hb.LHC7_CS_bb_hj_ratio(0) = bb / bb0;
  hb.LHC7_CS_tthj_ratio(0) = pow(cu, 2);      //< neglects ZH>ttH diagrams
  hb.LHC7_CS_thj_tchan_ratio(0) = pow(cu, 2); //< LO correct
  hb.LHC7_CS_thj_schan_ratio(0) = pow(cu, 2); //< LO correct
  //   LHC8
  gg = cxnH0_.GGo(p.mA, cu, cd, Tools::SushiTables::Collider::LHC8);
  bb = cxnH0_.BBo(p.mA, cd, Tools::SushiTables::Collider::LHC8);
  gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::LHC8);
  bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::LHC8);
  hb.LHC8_CS_hj_ratio(0) = (gg + bb) / (gg0 + bb0);
  hb.LHC8_CS_gg_hj_ratio(0) = gg / gg0;
  hb.LHC8_CS_bb_hj_ratio(0) = bb / bb0;
  hb.LHC8_CS_tthj_ratio(0) = pow(cu, 2);      //< neglects ZH>ttH diagrams
  hb.LHC8_CS_thj_tchan_ratio(0) = pow(cu, 2); //< LO correct
  hb.LHC8_CS_thj_schan_ratio(0) = pow(cu, 2); //< LO correct
  //   LHC13
  gg = cxnH0_.GGo(p.mA, cu, cd, Tools::SushiTables::Collider::LHC13);
  bb = cxnH0_.BBo(p.mA, cd, Tools::SushiTables::Collider::LHC13);
  gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::LHC13);
  bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::LHC13);
  hb.LHC13_CS_hj_ratio(0) = (gg + bb) / (gg0 + bb0);
  hb.LHC13_CS_gg_hj_ratio(0) = gg / gg0;
  hb.LHC13_CS_bb_hj_ratio(0) = bb / bb0;
  const auto x_VH = hbhs.GetVHCxns(p.mA, 0, {0, cu}, {0, cd});
  const auto x_VH_ref = hbhs.GetVHCxns(p.mA, 1, 1, 1);
  hb.LHC13_CS_hjZ_ratio(0) = x_VH.x_hZ / x_VH_ref.x_hZ;
  hb.LHC13_CS_gg_hjZ_ratio(0) = x_VH.x_gg_hZ / x_VH_ref.x_gg_hZ;
  hb.LHC13_CS_qq_hjZ_ratio(0) = x_VH.x_qq_hZ / x_VH_ref.x_qq_hZ;
  hb.LHC13_CS_hjW_ratio(0) = 0;
  hb.LHC13_CS_tthj_ratio(0) = pow(cu, 2);      //< neglects ZH>ttH diagrams
  hb.LHC13_CS_thj_tchan_ratio(0) = pow(cu, 2); //< LO correct
  hb.LHC13_CS_thj_schan_ratio(0) = pow(cu, 2); //< LO correct
  hb.LHC13_CS_tWhj_ratio(0) =
      Interfaces::HiggsBoundsSignals::tWHratio(0., {0, cu});

  // ---- charged Higgs ----
  hb.Mhplus(0) = p.mHp;
  hb.GammaTotal_Hpj(0) = p.data["w_Hp"];
  hb.CS_ee_HpjHmj_ratio(0) = 1;
  hb.BR_tWpb = p.data["b_t_Wb"];
  hb.BR_tHpjb(0) = p.data["b_t_Hpb"];
  hb.BR_Hpjcs(0) = p.data["b_Hp_cs"];
  hb.BR_Hpjcb(0) = p.data["b_Hp_cb"];
  hb.BR_Hpjtaunu(0) = p.data["b_Hp_taunu"];
  hb.BR_Hpjtb(0) = p.data["b_Hp_tb"];
  // BR_HpjWZ(0) == 0
  for (size_t i = 0; i != nHzero; ++i)
    hb.BR_HpjhiW(0, i) = p.data["b_Hp_W"s + namesHzero[i]];

  // charged Higgs cxn (as of Oct 2018 the only one needed)
  auto coupsHp = TwoHDM::TwoHDMHpCoups(p.tbeta, p.type);
  p.data.Store("x_tHpm", hbhs.GetHpCxn(p.mHp, coupsHp.rhot, coupsHp.rhob,
                                       p.data["b_t_Hpb"]));
  hb.LHC13_CS_Hpjtb(0) = p.data["x_tHpm"];
  return hb;
}

Constraints::STUDetail::STUParameters
N2HDMBroken::STUInput(const ParameterPoint &p) {
  Constraints::STUDetail::STUParameters res{
      Eigen::MatrixXcd(2, nHzero + 1),
      Eigen::MatrixXd(2, nHplus + 1),
      {p.mA, p.mHi[0], p.mHi[1], p.mHi[2]},
      {p.mHp}};
  const double cb = std::cos(std::atan(p.tbeta));
  const double sb = std::sin(std::atan(p.tbeta));

  res.mV << std::complex<double>(0, cb), std::complex<double>(0, -sb),
      p.R(0, 0), p.R(1, 0), p.R(2, 0), std::complex<double>(0, sb),
      std::complex<double>(0, cb), p.R(0, 1), p.R(1, 1), p.R(2, 1);
  res.mU << cb, -sb, sb, cb;
  return res;
}

bool N2HDMBroken::EWPValid(const ParameterPoint &p) {
  return (p.mHp + p.mHi[0] > Constants::mW) && (p.mHp + p.mA > Constants::mW) &&
         (p.mHi[0] + p.mA > Constants::mZ) && (2 * p.mHp > Constants::mZ);
}

// ---------------------------------- Theory ----------------------------------

std::vector<double> N2HDMBroken::ParamsEVADE(const ParameterPoint &p) {
  return {atan(p.tbeta), p.v,     p.vs,    p.L[0],  p.L[1],
          p.L[2],        p.L[3],  p.L[4],  p.L[5],  p.L[6],
          p.L[7],        p.m12sq, p.m11sq, p.m22sq, p.mssq};
}

const Tools::SushiTables N2HDMBroken::cxnH0_;

std::string N2HDMBroken::ParameterPoint::ToString() const {
  std::ostringstream os;
  auto printer = Utilities::TSVPrinter(os);
  for (double m : mHi) {
    printer << m;
  }
  printer << mA << mHp << tbeta;
  printer << R.format(Utilities::TSVPrinter::matrixFormat);
  for (double a : alpha)
    printer << a;
  printer << static_cast<int>(type);
  printer << vs;
  printer << v;
  printer << m12sq;
  for (double l : L)
    printer << l;
  printer << m11sq << m22sq << mssq;
  for (const auto &[key, value] : data) {
    printer << value;
  }
  return os.str();
}

void N2HDMBroken::CalcCouplings(ParameterPoint &p) {
  const double cb = std::cos(std::atan(p.tbeta));
  const double sb = std::sin(std::atan(p.tbeta));
  // SM couplings
  for (size_t i = 0; i != nHzero - 1; ++i) {
    p.data.Store("c_"s + namesHzero[i + 1] + "VV",
                 cb * p.R(i, 0) + sb * p.R(i, 1));
    p.data.Store("c_"s + namesHzero[i + 1] + "AZ",
                 (sb * p.R(i, 0) - cb * p.R(i, 1)));
    p.data.Store("c_"s + namesHzero[i + 1] + "uu_e", p.R(i, 1) / sb);
    if (p.type == Yuk::typeI) {
      p.data.Store("c_"s + namesHzero[i + 1] + "dd_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i + 1] + "ll_e", p.R(i, 1) / sb);
    }
    if (p.type == Yuk::typeII) {
      p.data.Store("c_"s + namesHzero[i + 1] + "dd_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i + 1] + "ll_e", p.R(i, 0) / cb);
    }
    if (p.type == Yuk::leptonSpecific) { // Lepton Specific
      p.data.Store("c_"s + namesHzero[i + 1] + "dd_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i + 1] + "ll_e", p.R(i, 0) / cb);
    }
    if (p.type == Yuk::flipped) { // flipped
      p.data.Store("c_"s + namesHzero[i + 1] + "dd_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i + 1] + "ll_e", p.R(i, 1) / sb);
    }
  }
  p.data.Store("c_Auu_o", 1 / p.tbeta);
  if (p.type == Yuk::typeI) {
    p.data.Store("c_Add_o", -1 / p.tbeta);
    p.data.Store("c_All_o", -1 / p.tbeta);
  }
  if (p.type == Yuk::typeII) {
    p.data.Store("c_Add_o", p.tbeta);
    p.data.Store("c_All_o", p.tbeta);
  }
  if (p.type == Yuk::leptonSpecific) {
    p.data.Store("c_Add_o", -1 / p.tbeta);
    p.data.Store("c_All_o", p.tbeta);
  }
  if (p.type == Yuk::flipped) {
    p.data.Store("c_Add_o", p.tbeta);
    p.data.Store("c_All_o", -1 / p.tbeta);
  }
}

void N2HDMBroken::CalcCXNs(ParameterPoint &p) {
  for (size_t i = 1; i != nHzero; ++i) {
    p.data.Store("x_"s + namesHzero[i] + "_ggH",
                 cxnH0_.GGe(p.mHi.at(i - 1),
                            p.data["c_"s + namesHzero[i] + "uu_e"],
                            p.data["c_"s + namesHzero[i] + "dd_e"],
                            Tools::SushiTables::Collider::LHC13));
    p.data.Store("x_"s + namesHzero[i] + "_bbH",
                 cxnH0_.BBe(p.mHi.at(i - 1),
                            p.data["c_"s + namesHzero[i] + "dd_e"],
                            Tools::SushiTables::Collider::LHC13));
  }
  p.data.Store("x_A_ggH", cxnH0_.GGo(p.mA, p.data["c_Auu_o"], p.data["c_Add_o"],
                                     Tools::SushiTables::Collider::LHC13));
  p.data.Store("x_A_bbH", cxnH0_.BBo(p.mA, p.data["c_Add_o"],
                                     Tools::SushiTables::Collider::LHC13));
}

std::vector<double> N2HDMBroken::BsmptInput(const ParameterPoint &p) {
  return {p.L[0],  p.L[1], p.L[2],  p.L[3],
          p.L[4],  p.L[5], p.L[6],  p.L[7],
          p.tbeta, p.vs,   p.m12sq, static_cast<double>(p.type)};
}

} // namespace ScannerS::Models
