#include "ScannerS/Models/C2HDM.hpp"

#include "AnyHdecay.hpp"
#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Interfaces/HiggsBoundsSignals.hpp"
#include "ScannerS/Tools/C2HEDM.hpp"
#include "ScannerS/Tools/SushiTables.hpp"
#include "ScannerS/Utilities.hpp"
#include <cmath>
#include <map>
#include <optional>
#include <sstream>
#include <utility>

namespace ScannerS::Models {

// -------------------------------- Generation --------------------------------
namespace {
std::array<double, 3> C2HDMThreeMasses(const std::array<double, 3> &alphain,
                                       double tbeta, double mHa, double mHb) {
  using std::pow;
  auto R = Utilities::MixMat3d(alphain[0], alphain[1], alphain[2]);
  double mHzsq = (pow(mHa, 2) * R(0, 2) * (R(0, 1) * tbeta - R(0, 0)) +
                  pow(mHb, 2) * R(1, 2) * (R(1, 1) * tbeta - R(1, 0))) /
                 (-R(2, 2) * (R(2, 1) * tbeta - R(2, 0)));
  return {mHa, mHb, mHzsq > 0 ? std::sqrt(mHzsq) : -1};
}

std::array<double, 6> Lambdas(double tbeta, double v,
                              const std::array<double, 3> &mHi, double mHp,
                              const Eigen::Matrix3d &R,
                              const std::array<double, 3> &alpha,
                              double re_m12sq) {
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  const double c1 = cos(alpha[0]);
  const double s1 = sin(alpha[0]);
  const double c2 = cos(alpha[1]);
  const double s2 = sin(alpha[1]);
  const double c3 = cos(alpha[2]);
  const double s3 = sin(alpha[2]);

  const double L1 = -1 / pow(cb * v, 2) *
                    (-pow(mHi[0] * R(0, 0), 2) - pow(mHi[1] * R(1, 0), 2) -
                     pow(mHi[2] * R(2, 0), 2) + re_m12sq * sb / cb);

  const double L2 = -1 / pow(sb * v, 2) *
                    (-pow(mHi[0] * R(0, 1), 2) - pow(mHi[1] * R(1, 1), 2) -
                     pow(mHi[2] * R(2, 1), 2) + re_m12sq * cb / sb);

  const double L3 =
      1 / pow(v, 2) *
      (1 / sb / cb *
           (c1 * s1 *
                (pow(mHi[0] * c2, 2) +
                 pow(mHi[1], 2) * (pow(s2 * s3, 2) - pow(c3, 2)) +
                 pow(mHi[2], 2) * (pow(s2 * c3, 2) - pow(s3, 2))) +
            s2 * c3 * s3 *
                ((pow(mHi[2], 2) - pow(mHi[1], 2)) *
                 (pow(c1, 2) - pow(s1, 2)))) -
       re_m12sq / sb / cb + 2 * pow(mHp, 2));

  const double L4 =
      1 / pow(v, 2) *
      (pow(mHi[0] * s2, 2) + pow(mHi[1] * c2 * s3, 2) +
       pow(mHi[2] * c2 * c3, 2) + re_m12sq / sb / cb - 2 * pow(mHp, 2));

  const double re_L5 = 1 / pow(v, 2) *
                       (-pow(mHi[0] * s2, 2) - pow(mHi[1] * c2 * s3, 2) -
                        pow(mHi[2] * c2 * c3, 2) + re_m12sq / sb / cb);

  const double im_L5 =
      2 * c2 / sb / pow(v, 2) *
      (c1 * s2 * (-pow(mHi[0], 2) + pow(mHi[1] * s3, 2) + pow(mHi[2] * c3, 2)) +
       s1 * s3 * c3 * (pow(mHi[1], 2) - pow(mHi[2], 2)));

  return {L1, L2, L3, L4, re_L5, im_L5};
}

double im_m12sq(double v, double tbeta, double im_L5) {
  using std::pow;
  return pow(v, 2) * tbeta / 2 / (1 + pow(tbeta, 2)) * im_L5;
}

double M11sq(double v, double tbeta, double L1, double L345, double re_m12sq) {
  using std::atan;
  using std::cos;
  using std::pow;
  using std::sin;
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  return 1 / 2. *
         (-pow(v * sb, 2) * L345 - pow(v * cb, 2) * L1 +
          2 * re_m12sq * sb / cb);
  ;
}

double M22sq(double v, double tbeta, double L2, double L345, double re_m12sq) {
  using std::atan;
  using std::cos;
  using std::pow;
  using std::sin;
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  return 1 / 2. *
         (-pow(v * cb, 2) * L345 - pow(v * sb, 2) * L2 +
          2 * re_m12sq * cb / sb);
}

std::optional<std::array<double, 3>>
PhysicalInputAngles(const C2HDM::PhysicalInput &in) {
  double c_HxVV = sqrt(in.c_HaVV_sq);
  const double Rx1 =
      (1 + in.c_HaVV_sq + (-in.c_Hatt_sq + in.c_HaVV_sq) * pow(in.tbeta, 2)) /
      (2. * c_HxVV * sqrt(1 + pow(in.tbeta, 2)));

  if (Rx1 < 0) {
    // Rx1 *= -1; // irrelevant
    c_HxVV *= -1;
  }

  const double Rx2 =
      (-1 + in.c_HaVV_sq + (in.c_Hatt_sq + in.c_HaVV_sq) * pow(in.tbeta, 2)) /
      (2. * c_HxVV * in.tbeta * sqrt(1 + pow(in.tbeta, 2)));

  if (Rx2 * c_HxVV < 0) // top and gauge coupling have the same sign
    return std::nullopt;
  if (Rx1 * Rx1 + Rx2 * Rx2 > 1) // unitarity
    return std::nullopt;
  const double Rx3 = sqrt(1 - Rx1 * Rx1 - Rx2 * Rx2) * in.sign_Ra3;

  if (Rx3 * Rx3 + in.Rb3 * in.Rb3 > 1) // unitarity
    return std::nullopt;

  const double a2 = asin(Rx3);
  const double a1 = asin(Rx2 / cos(a2));
  const double a3 = asin(in.Rb3 / cos(a2));

  return std::array<double, 3>{a1, a2, a3};
}

std::array<double, 3> PhysicalMasses(const C2HDM::PhysicalInput &in) {
  if (auto alphaOrError = PhysicalInputAngles(in)) {
    return Utilities::Sorted(
        C2HDMThreeMasses(alphaOrError.value(), in.tbeta, in.mHa, in.mHb));
  } else {
    return {-1, in.mHa, in.mHb};
  }
}

Eigen::Matrix3d PhysicalMixMat(const C2HDM::PhysicalInput &in) {
  if (auto alphaOrError = PhysicalInputAngles(in)) {
    auto alphain = alphaOrError.value();
    return Utilities::OrderedMixMat3d(
        alphain[0], alphain[1], alphain[2],
        C2HDMThreeMasses(alphain, in.tbeta, in.mHa, in.mHb));
  } else {
    return Eigen::Matrix3d::Constant(2);
  }
}

} // namespace

C2HDM::ParameterPoint::ParameterPoint(const AngleInput &in)
    : mHi{Utilities::Sorted(
          C2HDMThreeMasses({in.a1, in.a2, in.a3}, in.tbeta, in.mHa, in.mHb))},
      mHp{in.mHp}, tbeta{in.tbeta}, R{Utilities::OrderedMixMat3d(
                                        in.a1, in.a2, in.a3,
                                        C2HDMThreeMasses({in.a1, in.a2, in.a3},
                                                         in.tbeta, in.mHa,
                                                         in.mHb))},
      alpha{Utilities::MixMatAngles3d(R)}, L{Lambdas(tbeta, in.v, mHi, mHp, R,
                                                     alpha, in.re_m12sq)},
      m12sq{in.re_m12sq, im_m12sq(in.v, tbeta, L[5])},
      m11sq{M11sq(in.v, tbeta, L[0], L[2] + L[3] + L[4], m12sq.real())},
      m22sq{M22sq(in.v, tbeta, L[1], L[2] + L[3] + L[4], m12sq.real())},
      type{in.type}, v{in.v} {}

C2HDM::ParameterPoint::ParameterPoint(const PhysicalInput &in)
    : mHi{PhysicalMasses(in)}, mHp{in.mHp}, tbeta{in.tbeta}, R{PhysicalMixMat(
                                                                 in)},
      alpha{Utilities::MixMatAngles3d(R)}, L{Lambdas(tbeta, in.v, mHi, mHp, R,
                                                     alpha, in.re_m12sq)},
      m12sq{in.re_m12sq, im_m12sq(in.v, tbeta, L[5])},
      m11sq{M11sq(in.v, tbeta, L[0], L[2] + L[3] + L[4], m12sq.real())},
      m22sq{M22sq(in.v, tbeta, L[1], L[2] + L[3] + L[4], m12sq.real())},
      type{in.type}, v{in.v} {}

// -------------------------------- Pheno --------------------------------
void C2HDM::RunHdecay(ParameterPoint &p) {
  using namespace AnyHdecay;
  static const Hdecay hdec{};
  auto result =
      hdec.c2hdm(static_cast<Yukawa>(p.type), TanBeta{p.tbeta},
                 SquaredMassPar{p.m12sq.real()}, HcMass{p.mHp},
                 HMassCPM{p.mHi[0]}, HMassCPM{p.mHi[1]}, MixAngle{p.alpha[0]},
                 MixAngle{p.alpha[1]}, MixAngle{p.alpha[2]});
  for (auto [key, value] : result)
    p.data.Store(std::string(key), value);
}

Interfaces::HiggsBoundsSignals::HBInput<C2HDM::nHzero, C2HDM::nHplus>
C2HDM::HiggsBoundsInput(
    ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {
  Interfaces::HiggsBoundsSignals::HBInput<nHzero, nHplus> hb;
  for (size_t i = 0; i != nHzero; ++i) {
    hb.Mh(i) = p.mHi[i];
    hb.GammaTotal_hj(i) = p.data["w_"s + namesHzero[i]];
    hb.CP_value(i) = 0;
    // HiggsBounds_neutral_input_SMBR
    hb.BR_hjss(i) = p.data["b_"s + namesHzero[i] + "_ss"];
    hb.BR_hjcc(i) = p.data["b_"s + namesHzero[i] + "_cc"];
    hb.BR_hjbb(i) = p.data["b_"s + namesHzero[i] + "_bb"];
    hb.BR_hjtt(i) = p.data["b_"s + namesHzero[i] + "_tt"];
    hb.BR_hjmumu(i) = p.data["b_"s + namesHzero[i] + "_mumu"];
    hb.BR_hjtautau(i) = p.data["b_"s + namesHzero[i] + "_tautau"];
    hb.BR_hjWW(i) = p.data["b_"s + namesHzero[i] + "_WW"];
    hb.BR_hjZZ(i) = p.data["b_"s + namesHzero[i] + "_ZZ"];
    hb.BR_hjZga(i) = p.data["b_"s + namesHzero[i] + "_Zgam"];
    hb.BR_hjgaga(i) = p.data["b_"s + namesHzero[i] + "_gamgam"];
    hb.BR_hjgg(i) = p.data["b_"s + namesHzero[i] + "_gg"];
  }
  // HiggsBounds_neutral_input_nonSMBR
  hb.BR_hkhjhi(1, 0, 0) = p.data["b_H2_H1H1"];
  hb.BR_hkhjhi(2, 0, 0) = p.data["b_H3_H1H1"];
  hb.BR_hkhjhi(2, 1, 1) = p.data["b_H3_H2H2"];
  hb.BR_hkhjhi(2, 0, 1) = p.data["b_H3_H1H2"];
  hb.BR_hkhjhi(2, 1, 0) = hb.BR_hkhjhi(2, 0, 1);
  hb.BR_hjhiZ(1, 0) = p.data["b_H2_ZH1"];
  hb.BR_hjhiZ(2, 0) = p.data["b_H3_ZH1"];
  hb.BR_hjhiZ(2, 1) = p.data["b_H3_ZH2"];

  for (size_t i = 0; i != nHzero; ++i) {
    hb.BR_hjHpiW(i, 0) = p.data["b_"s + namesHzero[i] + "_WpHm"];
    // get the couplings
    const double cVV = p.data["c_"s + namesHzero[i] + "VV"];
    const double cu_e = p.data["c_"s + namesHzero[i] + "uu_e"];
    const double cu_o = p.data["c_"s + namesHzero[i] + "uu_o"];
    const double cd_e = p.data["c_"s + namesHzero[i] + "dd_e"];
    const double cd_o = p.data["c_"s + namesHzero[i] + "dd_o"];
    const double cl_e = p.data["c_"s + namesHzero[i] + "ll_e"];
    const double cl_o = p.data["c_"s + namesHzero[i] + "ll_o"];

    // HiggsBounds_neutral_input_LEP
    hb.XS_ee_hjZ_ratio(i) = pow(cVV, 2);
    hb.XS_ee_bbhj_ratio(i) = pow(cd_e, 2) + pow(cd_o, 2);
    hb.XS_ee_tautauhj_ratio(i) = pow(cl_e, 2) + pow(cl_o, 2);
    for (size_t j = i; j != nHzero; ++j) {
      hb.XS_ee_hjhi_ratio(i, j) =
          pow(p.data["c_"s + namesHzero[i] + namesHzero[j] + "Z"], 2);
      hb.XS_ee_hjhi_ratio(j, i) = hb.XS_ee_hjhi_ratio(i, j);
    }
    // HiggsBounds_neutral_input_hadr
    //   Tevatron
    double gg = cxnH0_.GG(p.mHi[i], cu_e, cd_e, cu_o, cd_o,
                          Tools::SushiTables::Collider::TEV);
    double bb =
        cxnH0_.BB(p.mHi[i], cd_e, cd_o, Tools::SushiTables::Collider::TEV);
    double gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::TEV);
    double bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::TEV);
    hb.TEV_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.TEV_CS_gg_hj_ratio(i) = gg / gg0;
    hb.TEV_CS_bb_hj_ratio(i) = bb / bb0;
    hb.TEV_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_tthj_ratio(i) =
        pow(cu_e, 2) + pow(cu_o, 2); //< neglects ZH>ttH diagrams
    hb.TEV_CS_thj_tchan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    hb.TEV_CS_thj_schan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    //   LHC7
    gg = cxnH0_.GG(p.mHi[i], cu_e, cd_e, cu_o, cd_o,
                   Tools::SushiTables::Collider::LHC7);
    bb = cxnH0_.BB(p.mHi[i], cd_e, cd_o, Tools::SushiTables::Collider::LHC7);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC7);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC7);
    hb.LHC7_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC7_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC7_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC7_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_tthj_ratio(i) =
        pow(cu_e, 2) + pow(cu_o, 2); //< neglects ZH>ttH diagrams
    hb.LHC7_CS_thj_tchan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    hb.LHC7_CS_thj_schan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    //   Tevatron
    gg = cxnH0_.GG(p.mHi[i], cu_e, cd_e, cu_o, cd_o,
                   Tools::SushiTables::Collider::LHC8);
    bb = cxnH0_.BB(p.mHi[i], cd_e, cd_o, Tools::SushiTables::Collider::LHC8);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC8);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC8);
    hb.LHC8_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC8_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC8_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC8_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_tthj_ratio(i) =
        pow(cu_e, 2) + pow(cu_o, 2); //< neglects ZH>ttH diagrams
    hb.LHC8_CS_thj_tchan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    hb.LHC8_CS_thj_schan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    //   Tevatron
    gg = cxnH0_.GG(p.mHi[i], cu_e, cd_e, cu_o, cd_o,
                   Tools::SushiTables::Collider::LHC13);
    bb = cxnH0_.BB(p.mHi[i], cd_e, cd_o, Tools::SushiTables::Collider::LHC13);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC13);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC13);
    hb.LHC13_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC13_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC13_CS_bb_hj_ratio(i) = bb / bb0;
    const auto x_VH = hbhs.GetVHCxns(p.mHi[i], cVV, {cu_e, cu_o}, {cd_e, cd_o});
    const auto x_VH_ref = hbhs.GetVHCxns(p.mHi[i], 1, 1, 1);
    hb.LHC13_CS_hjZ_ratio(i) = x_VH.x_hZ / x_VH_ref.x_hZ;
    hb.LHC13_CS_gg_hjZ_ratio(i) = x_VH.x_gg_hZ / x_VH_ref.x_gg_hZ;
    hb.LHC13_CS_qq_hjZ_ratio(i) = x_VH.x_qq_hZ / x_VH_ref.x_qq_hZ;
    hb.LHC13_CS_hjW_ratio(i) = x_VH.x_hW / x_VH_ref.x_hW;
    hb.LHC13_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC13_CS_tthj_ratio(i) =
        pow(cu_e, 2) + pow(cu_o, 2); //< neglects ZH>ttH diagrams
    hb.LHC13_CS_thj_tchan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    hb.LHC13_CS_thj_schan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    hb.LHC13_CS_tWhj_ratio(i) =
        Interfaces::HiggsBoundsSignals::tWHratio(cVV, {cu_e, cu_o});
  }
  hb.Mhplus(0) = p.mHp;
  hb.GammaTotal_Hpj(0) = p.data["w_Hp"];
  hb.CS_ee_HpjHmj_ratio(0) = 1;
  hb.BR_tWpb = p.data["b_t_Wb"];
  hb.BR_tHpjb(0) = p.data["b_t_Hpb"];
  hb.BR_Hpjcs(0) = p.data["b_Hp_cs"];
  hb.BR_Hpjcb(0) = p.data["b_Hp_cb"];
  hb.BR_Hpjtaunu(0) = p.data["b_Hp_taunu"];
  hb.BR_Hpjtb(0) = p.data["b_Hp_tb"];
  // BR_HpjWZ(0) == 0
  for (size_t i = 0; i != nHzero; ++i)
    hb.BR_HpjhiW(0, i) = p.data["b_Hp_W"s + namesHzero[i]];

  // charged Higgs cxn (as of Jan 2020 the only one needed)
  auto coupsHp = TwoHDMHpCoups(p.tbeta, p.type);
  p.data.Store("x_tHpm", hbhs.GetHpCxn(p.mHp, coupsHp.rhot, coupsHp.rhob,
                                       p.data["b_t_Hpb"]));
  hb.LHC13_CS_Hpjtb(0) = p.data["x_tHpm"];
  return hb;
}

Constraints::STUDetail::STUParameters C2HDM::STUInput(const ParameterPoint &p) {
  Constraints::STUDetail::STUParameters res{Eigen::MatrixXcd(2, nHzero + 1),
                                            Eigen::Matrix2d(),
                                            {p.mHi[0], p.mHi[1], p.mHi[2]},
                                            {p.mHp}};
  const double cb = cos(atan(p.tbeta));
  const double sb = sin(atan(p.tbeta));
  res.mU << cb, -sb, sb, cb;

  res.mV(0, 0) = {0, cb};
  res.mV(0, 1) = {p.R(0, 0), -sb * p.R(0, 2)};
  res.mV(0, 2) = {p.R(1, 0), -sb * p.R(1, 2)};
  res.mV(0, 3) = {p.R(2, 0), -sb * p.R(2, 2)};

  res.mV(1, 0) = {0, sb};
  res.mV(1, 1) = {p.R(0, 1), cb * p.R(0, 2)};
  res.mV(1, 2) = {p.R(1, 1), cb * p.R(1, 2)};
  res.mV(1, 3) = {p.R(2, 1), cb * p.R(2, 2)};

  return res;
}

bool C2HDM::EWPValid(const ParameterPoint &p) {
  return (p.mHp + p.mHi[0] > Constants::mW) && (2 * p.mHi[0] > Constants::mZ) &&
         (2 * p.mHp > Constants::mZ);
}

double C2HDM::CalcElectronEDM(ParameterPoint &p) {
  auto in = Tools::C2HEDMInput<nHzero>{};
  in.mHp = p.mHp;
  in.mHi = p.mHi;
  for (size_t i = 0; i != nHzero; ++i) {
    in.c_Hff[i][0][0] = p.data["c_"s + namesHzero[i] + "uu_e"];
    in.c_Hff[i][0][1] = p.data["c_"s + namesHzero[i] + "uu_o"];
    in.c_Hff[i][1][0] = p.data["c_"s + namesHzero[i] + "dd_e"];
    in.c_Hff[i][1][1] = p.data["c_"s + namesHzero[i] + "dd_o"];
    in.c_Hff[i][2][0] = p.data["c_"s + namesHzero[i] + "ll_e"];
    in.c_Hff[i][2][1] = p.data["c_"s + namesHzero[i] + "ll_o"];
    in.c_HVV[i] = p.data["c_"s + namesHzero[i] + "VV"];
    in.c_HHpHm[i] = p.data["c_"s + namesHzero[i] + "HpHm"];
  }
  auto result = ScannerS::Tools::calcElectronEDM(in);
  p.data.Store("edm_e_f", result.contribF);
  p.data.Store("edm_e_Hp", result.contribHp);
  p.data.Store("edm_e_W", result.contribW);
  p.data.Store("edm_e_HpW", result.contribHpW);
  return result.value;
}

// ---------------------------------- Theory ----------------------------------
bool C2HDM::BFB(const std::array<double, 6> &L) {
  return TwoHDM::BFB(L[0], L[1], L[2], L[3], std::hypot(L[4], L[5]));
}

double C2HDM::MaxUnitarityEV(const std::array<double, 6> &L) {
  return TwoHDM::MaxUnitarityEV(L[0], L[1], L[2], L[3], std::hypot(L[4], L[5]));
}

bool C2HDM::AbsoluteStability(const ParameterPoint &p) {
  using std::pow;
  using std::sqrt;
  const double disc =
      (pow(p.mHp * p.mHp / p.v / p.v + p.L[3] / 2., 2) -
       (p.L[4] * p.L[4] + p.L[5] * p.L[5]) / 4.) *
      (p.mHp * p.mHp / p.v / p.v + (sqrt(p.L[0] * p.L[1]) - p.L[2]) / 2.);
  return disc > 0;
}

// -------------------------------- Properties --------------------------------
const Tools::SushiTables C2HDM::cxnH0_{};

std::string C2HDM::ParameterPoint::ToString() const {
  std::ostringstream os;
  auto printer = Utilities::TSVPrinter(os);
  for (double m : mHi)
    printer << m;
  printer << mHp << tbeta << m12sq.real() << m12sq.imag();
  for (double l : L)
    printer << l;
  printer << m11sq << m22sq;
  printer << R.format(Utilities::TSVPrinter::matrixFormat);
  printer << static_cast<int>(type);
  printer << v;
  for (double a : alpha)
    printer << a;
  for (const auto &[key, value] : data)
    printer << value;
  return os.str();
}

void C2HDM::CalcCouplings(ParameterPoint &p) {
  const double cb = std::cos(std::atan(p.tbeta));
  const double sb = std::sin(std::atan(p.tbeta));
  // gauge couplings
  for (size_t i = 0; i != nHzero; ++i) {
    p.data.Store("c_"s + namesHzero[i] + "VV", cb * p.R(i, 0) + sb * p.R(i, 1));
    for (size_t j = i; j != nHzero; ++j) {
      p.data.Store("c_"s + namesHzero[i] + namesHzero[j] + "Z",
                   (sb * p.R(i, 0) - cb * p.R(i, 1)) * p.R(j, 2) -
                       (sb * p.R(j, 0) - cb * p.R(j, 1)) * p.R(i, 2));
    }
  }
  // fermion couplings
  for (size_t i = 0; i != nHzero; i++) {
    p.data.Store("c_"s + namesHzero[i] + "uu_e", p.R(i, 1) / sb);
    p.data.Store("c_"s + namesHzero[i] + "uu_o", -p.R(i, 2) / p.tbeta);
    if (p.type == Yuk::typeI) {
      p.data.Store("c_"s + namesHzero[i] + "dd_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i] + "dd_o", p.R(i, 2) / p.tbeta);
      p.data.Store("c_"s + namesHzero[i] + "ll_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i] + "ll_o", p.R(i, 2) / p.tbeta);
    }
    if (p.type == Yuk::typeII) {
      p.data.Store("c_"s + namesHzero[i] + "dd_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i] + "dd_o", -p.R(i, 2) * p.tbeta);
      p.data.Store("c_"s + namesHzero[i] + "ll_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i] + "ll_o", -p.R(i, 2) * p.tbeta);
    }
    if (p.type == Yuk::leptonSpecific) { // Lepton Specific
      p.data.Store("c_"s + namesHzero[i] + "dd_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i] + "dd_o", p.R(i, 2) / p.tbeta);
      p.data.Store("c_"s + namesHzero[i] + "ll_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i] + "ll_o", -p.R(i, 2) * p.tbeta);
    }
    if (p.type == Yuk::flipped) { // flipped
      p.data.Store("c_"s + namesHzero[i] + "dd_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i] + "dd_o", -p.R(i, 2) * p.tbeta);
      p.data.Store("c_"s + namesHzero[i] + "ll_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i] + "ll_o", p.R(i, 2) / p.tbeta);
    }
  }

  // charged H+ H- Hi couplings
  for (size_t i = 0; i != nHzero; ++i) {
    p.data.Store(
        "c_"s + namesHzero[i] + "HpHm",
        -(cb * (sb * sb * (p.L[0] - p.L[3] - p.L[4]) + cb * cb * p.L[2]) *
              p.R(i, 0) +
          sb * (cb * cb * (p.L[1] - p.L[3] - p.L[4]) + sb * sb * p.L[2]) *
              p.R(i, 1) +
          cb * sb * p.L[5] * p.R(i, 2)));
  }
}

void C2HDM::CalcCXNs(ParameterPoint &p) {
  for (size_t i = 0; i != nHzero; ++i) {
    p.data.Store("x_"s + namesHzero[i] + "_ggH",
                 cxnH0_.GG(p.mHi[i], p.data["c_"s + namesHzero[i] + "uu_e"],
                           p.data["c_"s + namesHzero[i] + "dd_e"],
                           p.data["c_"s + namesHzero[i] + "uu_o"],
                           p.data["c_"s + namesHzero[i] + "dd_o"],
                           Tools::SushiTables::Collider::LHC13));
    p.data.Store("x_"s + namesHzero[i] + "_bbH",
                 cxnH0_.BB(p.mHi[i], p.data["c_"s + namesHzero[i] + "dd_e"],
                           p.data["c_"s + namesHzero[i] + "dd_o"],
                           Tools::SushiTables::Collider::LHC13));
  }
}

std::vector<double> C2HDM::BsmptInput(const ParameterPoint &p) {
  return {p.L[0],         p.L[1],  p.L[2],
          p.L[3],         p.L[4],  p.L[5],
          p.m12sq.real(), p.tbeta, static_cast<double>(p.type)};
}

} // namespace ScannerS::Models
