#include "ScannerS/Models/N2HDM.hpp"

#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Utilities.hpp"
#include <algorithm>
#include <cmath>
#include <complex>
#include <cstdlib>

namespace ScannerS {
namespace Models {
bool N2HDM::BFB(const std::array<double, 8> &L) {
  const double D = std::min(L[3] - abs(L[4]), 0.);

  const bool reg1 = (L[0] > 0) && (L[1] > 0) && (L[5] > 0) &&
                    (std::sqrt(L[0] * L[5]) + L[6] > 0) &&
                    (std::sqrt(L[1] * L[5]) + L[7] > 0) &&
                    (std::sqrt(L[0] * L[1]) + L[2] + D > 0) &&
                    (L[6] + std::sqrt(L[0] / L[1]) * L[7] >= 0);

  const bool reg2 =
      (L[0] > 0) && (L[1] > 0) && (L[5] > 0) && (L[1] * L[5] >= L[7] * L[7]) &&
      (L[6] + std::sqrt(L[0] / L[1]) * L[7] <= 0) &&
      (std::sqrt(L[0] * L[5]) + L[6] > 0) &&
      (std::sqrt((L[6] * L[6] - L[0] * L[5]) * (L[7] * L[7] - L[1] * L[5])) >
       L[6] * L[7] - (D + L[2]) * L[5]);

  return reg1 || reg2;
}

double N2HDM::MaxUnitarityEV(const std::array<double, 8> &L) {
  // condition names refer to 1612.01309
  using ScannerS::Constants::pi;
  using std::abs, std::pow, std::sqrt;
  const auto maxEVinitial = std::max({
      abs(1 / 2. *
          (L[0] + L[1] + sqrt(pow(L[0] - L[1], 2) + 4 * pow(L[4], 2)))), // b+
      abs(1 / 2. *
          (L[0] + L[1] + sqrt(pow(L[0] - L[1], 2) + 4 * pow(L[3], 2)))), // c+
      abs(L[2] + 2 * L[3]) + abs(3 * L[4]), // e1 && f+
      abs(L[2] - L[3]),                     // p1
      abs(L[6]),                            // s1
      abs(L[7])                             // s2

  });
  // cubic polynomial of form x^3 + a x^2 + b x + c
  const double a = -3 * (2 * (L[0] + L[1]) + L[5]);
  const double b = 36 * L[0] * L[1] + 18 * (L[0] + L[1]) * L[5] -
                   4 * (pow(2 * L[2] + L[3], 2) + pow(L[6], 2) + pow(L[7], 2));
  const double c =
      -4 * (27 * L[0] * L[1] * L[5] - 3 * pow(2 * L[2] + L[3], 2) * L[5] -
            6 * L[1] * pow(L[6], 2) + 4 * (2 * L[2] + L[3]) * L[6] * L[7] -
            6 * L[0] * pow(L[7], 2));
  auto roots = Utilities::CubicRoots(a, b, c);
  return std::max(maxEVinitial, 1 / 2. * Utilities::AbsMax(roots));
}

Constraints::STUDetail::STUParameters N2HDM::STUInput(double mA,
                                           const std::array<double, 3> &mHi,
                                           double mHp, double tbeta,
                                           const Eigen::Matrix3d &R) {
  Constraints::STUDetail::STUParameters res{Eigen::MatrixXcd(2, nHzero + 1),
                                 Eigen::MatrixXd(2, nHplus + 1),
                                 {mA, mHi[0], mHi[1], mHi[2]},
                                 {mHp}};
  const double cb = std::cos(std::atan(tbeta));
  const double sb = std::sin(std::atan(tbeta));

  res.mV << std::complex<double>(0, cb), std::complex<double>(0, -sb), R(0, 0),
      R(1, 0), R(2, 0), std::complex<double>(0, sb),
      std::complex<double>(0, cb), R(0, 1), R(1, 1), R(2, 1);
  res.mU << cb, -sb, sb, cb;
  return res;
}
} // namespace Models
} // namespace ScannerS
