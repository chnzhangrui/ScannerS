#include "ScannerS/Models/TRSM.hpp"

#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Utilities.hpp"
#include <algorithm>
#include <cmath>

namespace ScannerS {
namespace Models {
bool TRSM::BFB(const std::array<double, 6> &L) {
  using std::sqrt;
  const double lHSbar = L[3] + 2 * sqrt(L[1] * L[0]);
  const double lHXbar = L[4] + 2 * sqrt(L[2] * L[0]);
  const double lSXbar = L[5] + 2 * sqrt(L[1] * L[2]);

  return L[1] > 0 && L[2] > 0 && L[0] > 0 && lHSbar > 0 && lHXbar > 0 &&
         lSXbar > 0 &&
         (sqrt(L[1]) * L[4] + sqrt(L[2]) * L[3] + sqrt(L[0]) * L[5] +
          sqrt(L[1] * L[2] * L[0]) + sqrt(lHSbar * lHXbar * lSXbar)) > 0;
}

double TRSM::MaxUnitarityEV(const std::array<double, 6> &L) {
  using ScannerS::Constants::pi;
  using std::abs, std::pow, std::sqrt;
  const auto maxEVinitial =
      std::max({abs(2 * L[0]), abs(L[3]), abs(L[4]), abs(L[5])});
  // cubic polynomial of form x^3 + a x^2 + b x + c
  const double a = -12 * L[0] - 6 * L[1] - 6 * L[2];
  const double b = 72 * L[0] * L[1] + 72 * L[0] * L[2] + 36 * L[1] * L[2] -
                   4 * pow(L[3], 2) - 4 * pow(L[4], 2) - pow(L[5], 2);
  const double c = -432 * L[0] * L[1] * L[2] + 24 * L[2] * pow(L[3], 2) +
                   24 * L[1] * pow(L[4], 2) - 8 * L[3] * L[4] * L[5] +
                   12 * L[0] * pow(L[5], 2);
  auto roots = Utilities::CubicRoots(a, b, c);
  return std::max(maxEVinitial, 1 / 2. * Utilities::AbsMax(roots));
}

Constraints::STUDetail::STUParameters
TRSM::STUInput(const std::array<double, 3> &mHi,
               const std::array<double, 3> &Ri0) {
  Constraints::STUDetail::STUParameters res{
      Eigen::MatrixXcd(1, nHzero + 1),
      Eigen::Matrix<double, 1, 1>::Constant(1),
      {mHi[0], mHi[1], mHi[2]},
      {}};
  res.mV(0, 0) = {0, 1};
  res.mV(0, 1) = {Ri0[0], 0};
  res.mV(0, 2) = {Ri0[1], 0};
  res.mV(0, 3) = {Ri0[2], 0};
  return res;
}

DataMap::Map TRSM::TripleHCoups(const std::array<double, 6> &L,
                                const Eigen::Matrix3d &R, double v, double vs,
                                double vx) {
  auto coups3H = DataMap::Map{};
  coups3H["c_H1H1H3"] = 2 * (3 * pow(R(0, 0), 2) * R(2, 0) * v * L[0] +
                             (pow(R(0, 1), 2) * R(2, 0) * v * L[3]) / 2. +
                             R(0, 0) * R(0, 1) * R(2, 1) * v * L[3] +
                             R(0, 0) * R(0, 1) * R(2, 0) * vs * L[3] +
                             (pow(R(0, 0), 2) * R(2, 1) * vs * L[3]) / 2. +
                             (pow(R(0, 2), 2) * R(2, 0) * v * L[4]) / 2. +
                             R(0, 0) * R(0, 2) * R(2, 2) * v * L[4] +
                             R(0, 0) * R(0, 2) * R(2, 0) * vx * L[4] +
                             (pow(R(0, 0), 2) * R(2, 2) * vx * L[4]) / 2. +
                             3 * pow(R(0, 1), 2) * R(2, 1) * vs * L[1] +
                             (pow(R(0, 2), 2) * R(2, 1) * vs * L[5]) / 2. +
                             R(0, 1) * R(0, 2) * R(2, 2) * vs * L[5] +
                             R(0, 1) * R(0, 2) * R(2, 1) * vx * L[5] +
                             (pow(R(0, 1), 2) * R(2, 2) * vx * L[5]) / 2. +
                             3 * pow(R(0, 2), 2) * R(2, 2) * vx * L[2]);
  coups3H["c_H1H2H2"] = 2 * (3 * R(0, 0) * pow(R(1, 0), 2) * v * L[0] +
                             R(0, 1) * R(1, 0) * R(1, 1) * v * L[3] +
                             (R(0, 0) * pow(R(1, 1), 2) * v * L[3]) / 2. +
                             (R(0, 1) * pow(R(1, 0), 2) * vs * L[3]) / 2. +
                             R(0, 0) * R(1, 0) * R(1, 1) * vs * L[3] +
                             R(0, 2) * R(1, 0) * R(1, 2) * v * L[4] +
                             (R(0, 0) * pow(R(1, 2), 2) * v * L[4]) / 2. +
                             (R(0, 2) * pow(R(1, 0), 2) * vx * L[4]) / 2. +
                             R(0, 0) * R(1, 0) * R(1, 2) * vx * L[4] +
                             3 * R(0, 1) * pow(R(1, 1), 2) * vs * L[1] +
                             R(0, 2) * R(1, 1) * R(1, 2) * vs * L[5] +
                             (R(0, 1) * pow(R(1, 2), 2) * vs * L[5]) / 2. +
                             (R(0, 2) * pow(R(1, 1), 2) * vx * L[5]) / 2. +
                             R(0, 1) * R(1, 1) * R(1, 2) * vx * L[5] +
                             3 * R(0, 2) * pow(R(1, 2), 2) * vx * L[2]);
  coups3H["c_H1H2H3"] = 6 * R(0, 0) * R(1, 0) * R(2, 0) * v * L[0] +
                        R(0, 1) * R(1, 1) * R(2, 0) * v * L[3] +
                        R(0, 1) * R(1, 0) * R(2, 1) * v * L[3] +
                        R(0, 0) * R(1, 1) * R(2, 1) * v * L[3] +
                        R(0, 1) * R(1, 0) * R(2, 0) * vs * L[3] +
                        R(0, 0) * R(1, 1) * R(2, 0) * vs * L[3] +
                        R(0, 0) * R(1, 0) * R(2, 1) * vs * L[3] +
                        R(0, 2) * R(1, 2) * R(2, 0) * v * L[4] +
                        R(0, 2) * R(1, 0) * R(2, 2) * v * L[4] +
                        R(0, 0) * R(1, 2) * R(2, 2) * v * L[4] +
                        R(0, 2) * R(1, 0) * R(2, 0) * vx * L[4] +
                        R(0, 0) * R(1, 2) * R(2, 0) * vx * L[4] +
                        R(0, 0) * R(1, 0) * R(2, 2) * vx * L[4] +
                        6 * R(0, 1) * R(1, 1) * R(2, 1) * vs * L[1] +
                        R(0, 2) * R(1, 2) * R(2, 1) * vs * L[5] +
                        R(0, 2) * R(1, 1) * R(2, 2) * vs * L[5] +
                        R(0, 1) * R(1, 2) * R(2, 2) * vs * L[5] +
                        R(0, 2) * R(1, 1) * R(2, 1) * vx * L[5] +
                        R(0, 1) * R(1, 2) * R(2, 1) * vx * L[5] +
                        R(0, 1) * R(1, 1) * R(2, 2) * vx * L[5] +
                        6 * R(0, 2) * R(1, 2) * R(2, 2) * vx * L[2];
  coups3H["c_H1H3H3"] = 2 * (3 * R(0, 0) * pow(R(2, 0), 2) * v * L[0] +
                             R(0, 1) * R(2, 0) * R(2, 1) * v * L[3] +
                             (R(0, 0) * pow(R(2, 1), 2) * v * L[3]) / 2. +
                             (R(0, 1) * pow(R(2, 0), 2) * vs * L[3]) / 2. +
                             R(0, 0) * R(2, 0) * R(2, 1) * vs * L[3] +
                             R(0, 2) * R(2, 0) * R(2, 2) * v * L[4] +
                             (R(0, 0) * pow(R(2, 2), 2) * v * L[4]) / 2. +
                             (R(0, 2) * pow(R(2, 0), 2) * vx * L[4]) / 2. +
                             R(0, 0) * R(2, 0) * R(2, 2) * vx * L[4] +
                             3 * R(0, 1) * pow(R(2, 1), 2) * vs * L[1] +
                             R(0, 2) * R(2, 1) * R(2, 2) * vs * L[5] +
                             (R(0, 1) * pow(R(2, 2), 2) * vs * L[5]) / 2. +
                             (R(0, 2) * pow(R(2, 1), 2) * vx * L[5]) / 2. +
                             R(0, 1) * R(2, 1) * R(2, 2) * vx * L[5] +
                             3 * R(0, 2) * pow(R(2, 2), 2) * vx * L[2]);
  coups3H["c_H1H1H2"] = 2 * (3 * pow(R(0, 0), 2) * R(1, 0) * v * L[0] +
                             (pow(R(0, 1), 2) * R(1, 0) * v * L[3]) / 2. +
                             R(0, 0) * R(0, 1) * R(1, 1) * v * L[3] +
                             R(0, 0) * R(0, 1) * R(1, 0) * vs * L[3] +
                             (pow(R(0, 0), 2) * R(1, 1) * vs * L[3]) / 2. +
                             (pow(R(0, 2), 2) * R(1, 0) * v * L[4]) / 2. +
                             R(0, 0) * R(0, 2) * R(1, 2) * v * L[4] +
                             R(0, 0) * R(0, 2) * R(1, 0) * vx * L[4] +
                             (pow(R(0, 0), 2) * R(1, 2) * vx * L[4]) / 2. +
                             3 * pow(R(0, 1), 2) * R(1, 1) * vs * L[1] +
                             (pow(R(0, 2), 2) * R(1, 1) * vs * L[5]) / 2. +
                             R(0, 1) * R(0, 2) * R(1, 2) * vs * L[5] +
                             R(0, 1) * R(0, 2) * R(1, 1) * vx * L[5] +
                             (pow(R(0, 1), 2) * R(1, 2) * vx * L[5]) / 2. +
                             3 * pow(R(0, 2), 2) * R(1, 2) * vx * L[2]);
  coups3H["c_H1H1H1"] = 6 * (pow(R(0, 0), 3) * v * L[0] +
                             (R(0, 0) * pow(R(0, 1), 2) * v * L[3]) / 2. +
                             (pow(R(0, 0), 2) * R(0, 1) * vs * L[3]) / 2. +
                             (R(0, 0) * pow(R(0, 2), 2) * v * L[4]) / 2. +
                             (pow(R(0, 0), 2) * R(0, 2) * vx * L[4]) / 2. +
                             pow(R(0, 1), 3) * vs * L[1] +
                             (R(0, 1) * pow(R(0, 2), 2) * vs * L[5]) / 2. +
                             (pow(R(0, 1), 2) * R(0, 2) * vx * L[5]) / 2. +
                             pow(R(0, 2), 3) * vx * L[2]);
  coups3H["c_H2H2H2"] = 6 * (pow(R(1, 0), 3) * v * L[0] +
                             (R(1, 0) * pow(R(1, 1), 2) * v * L[3]) / 2. +
                             (pow(R(1, 0), 2) * R(1, 1) * vs * L[3]) / 2. +
                             (R(1, 0) * pow(R(1, 2), 2) * v * L[4]) / 2. +
                             (pow(R(1, 0), 2) * R(1, 2) * vx * L[4]) / 2. +
                             pow(R(1, 1), 3) * vs * L[1] +
                             (R(1, 1) * pow(R(1, 2), 2) * vs * L[5]) / 2. +
                             (pow(R(1, 1), 2) * R(1, 2) * vx * L[5]) / 2. +
                             pow(R(1, 2), 3) * vx * L[2]);
  coups3H["c_H2H2H3"] = 2 * (3 * pow(R(1, 0), 2) * R(2, 0) * v * L[0] +
                             (pow(R(1, 1), 2) * R(2, 0) * v * L[3]) / 2. +
                             R(1, 0) * R(1, 1) * R(2, 1) * v * L[3] +
                             R(1, 0) * R(1, 1) * R(2, 0) * vs * L[3] +
                             (pow(R(1, 0), 2) * R(2, 1) * vs * L[3]) / 2. +
                             (pow(R(1, 2), 2) * R(2, 0) * v * L[4]) / 2. +
                             R(1, 0) * R(1, 2) * R(2, 2) * v * L[4] +
                             R(1, 0) * R(1, 2) * R(2, 0) * vx * L[4] +
                             (pow(R(1, 0), 2) * R(2, 2) * vx * L[4]) / 2. +
                             3 * pow(R(1, 1), 2) * R(2, 1) * vs * L[1] +
                             (pow(R(1, 2), 2) * R(2, 1) * vs * L[5]) / 2. +
                             R(1, 1) * R(1, 2) * R(2, 2) * vs * L[5] +
                             R(1, 1) * R(1, 2) * R(2, 1) * vx * L[5] +
                             (pow(R(1, 1), 2) * R(2, 2) * vx * L[5]) / 2. +
                             3 * pow(R(1, 2), 2) * R(2, 2) * vx * L[2]);
  coups3H["c_H2H3H3"] = 2 * (3 * R(1, 0) * pow(R(2, 0), 2) * v * L[0] +
                             R(1, 1) * R(2, 0) * R(2, 1) * v * L[3] +
                             (R(1, 0) * pow(R(2, 1), 2) * v * L[3]) / 2. +
                             (R(1, 1) * pow(R(2, 0), 2) * vs * L[3]) / 2. +
                             R(1, 0) * R(2, 0) * R(2, 1) * vs * L[3] +
                             R(1, 2) * R(2, 0) * R(2, 2) * v * L[4] +
                             (R(1, 0) * pow(R(2, 2), 2) * v * L[4]) / 2. +
                             (R(1, 2) * pow(R(2, 0), 2) * vx * L[4]) / 2. +
                             R(1, 0) * R(2, 0) * R(2, 2) * vx * L[4] +
                             3 * R(1, 1) * pow(R(2, 1), 2) * vs * L[1] +
                             R(1, 2) * R(2, 1) * R(2, 2) * vs * L[5] +
                             (R(1, 1) * pow(R(2, 2), 2) * vs * L[5]) / 2. +
                             (R(1, 2) * pow(R(2, 1), 2) * vx * L[5]) / 2. +
                             R(1, 1) * R(2, 1) * R(2, 2) * vx * L[5] +
                             3 * R(1, 2) * pow(R(2, 2), 2) * vx * L[2]);
  coups3H["c_H3H3H3"] = 6 * (pow(R(2, 0), 3) * v * L[0] +
                             (R(2, 0) * pow(R(2, 1), 2) * v * L[3]) / 2. +
                             (pow(R(2, 0), 2) * R(2, 1) * vs * L[3]) / 2. +
                             (R(2, 0) * pow(R(2, 2), 2) * v * L[4]) / 2. +
                             (pow(R(2, 0), 2) * R(2, 2) * vx * L[4]) / 2. +
                             pow(R(2, 1), 3) * vs * L[1] +
                             (R(2, 1) * pow(R(2, 2), 2) * vs * L[5]) / 2. +
                             (pow(R(2, 1), 2) * R(2, 2) * vx * L[5]) / 2. +
                             pow(R(2, 2), 3) * vx * L[2]);
  return coups3H;
}
} // namespace Models
} // namespace ScannerS
