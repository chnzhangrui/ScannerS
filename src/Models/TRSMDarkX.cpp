#include "ScannerS/Models/TRSMDarkX.hpp"

#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include <algorithm>
#include <cmath>

namespace ScannerS::Models {

// -------------------------------- Properties --------------------------------
namespace {
double CalcSortedAlpha(double mHx, double mHy, double alphaIn) {
  using ScannerS::Constants::pi;
  if (mHx >= mHy)
    return alphaIn;
  else {
    if (sin(alphaIn) > 0)
      return alphaIn - pi / 2;
    else
      return alphaIn + pi / 2;
  }
}

std::array<double, 6> CalcLambdas(const std::array<double, 2> &mHi,
                                  double alpha, double v, double vs,
                                  double lamX, double lamHX, double lamSX) {
  const double mH1sq = mHi[0] * mHi[0];
  const double mH2sq = mHi[1] * mHi[1];
  const double R11 = cos(alpha);
  const double R12 = sin(alpha);
  const double R21 = -R12;
  const double R22 = R11;

  const double lH =
      (mH1sq * pow(R11, 2) + mH2sq * pow(R21, 2)) / (2. * pow(v, 2));
  const double lS =
      (mH1sq * pow(R12, 2) + mH2sq * pow(R22, 2)) / (2. * pow(vs, 2));
  const double lHS = (mH1sq * R11 * R12 + mH2sq * R21 * R22) / (v * vs);
  return {lH, lS, lamX, lHS, lamHX, lamSX};
}

double CalcMuHsq(const std::array<double, 6> &L, double v, double vs) {
  using std::pow;
  return -(pow(v, 2) * L[0]) - (pow(vs, 2) * L[3]) / 2.;
}

double CalcMuSsq(const std::array<double, 6> &L, double v, double vs) {
  using std::pow;
  return -(pow(vs, 2) * L[1]) - (pow(v, 2) * L[3]) / 2.;
}

double CalcMuXsq(double mHD, const std::array<double, 6> &L, double v,
                 double vs) {
  using std::pow;
  return pow(mHD, 2) - (pow(v, 2) * L[4]) / 2. - (pow(vs, 2) * L[5]) / 2.;
}

} // namespace

TRSMDarkX::ParameterPoint::ParameterPoint(const AngleInput &in)
    : mHl{std::min({in.mHa, in.mHb})}, mHh{std::max({in.mHa, in.mHb})},
      mHD{in.mHX}, alpha{CalcSortedAlpha(in.mHa, in.mHb, in.a)}, v{in.v},
      vs{in.vs}, L{CalcLambdas({mHl, mHh}, alpha, v, vs, in.lamX, in.lamHX,
                               in.lamSX)},
      muHsq{CalcMuHsq(L, v, vs)}, muSsq{CalcMuSsq(L, v, vs)}, muXsq{CalcMuXsq(
                                                                  mHD, L, v,
                                                                  vs)} {}

// -------------------------------- Pheno --------------------------------
Constraints::STUDetail::STUParameters
TRSMDarkX::STUInput(const ParameterPoint &p) {
  return TRSM::STUInput({p.mHl, p.mHh, p.mHD},
                        {cos(p.alpha), -sin(p.alpha), 0});
}

} // namespace ScannerS::Models
