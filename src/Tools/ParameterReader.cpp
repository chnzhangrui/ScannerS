#include "ScannerS/Tools/ParameterReader.hpp"

#include <algorithm>
#include <iterator>
#include <sstream> // IWYU pragma: keep
#include <stdexcept>
#include <string>

namespace ScannerS::Tools {

namespace {
size_t CountLines(const std::string &filepath) {
  size_t n = 0;

  std::string s;

  std::ifstream linecount(filepath);
  while (!linecount.eof()) {
    std::getline(linecount, s);
    ++n;
  }
  if (s.empty()) { // file ends with newline
    return n - 1;
  }
  return n;
}
} // namespace

ParameterReader::ParameterReader(const std::string &filepath, size_t nParams)
    : file_{filepath, std::ifstream::in} {
  if (file_.fail()) {
    throw std::runtime_error("ParameterReader cannot open the input file " +
                             filepath);
  }
  nPoints_ = CountLines(filepath) - 1;

  columns_.resize(nParams);
  size_t i = 0;
  for (auto &x : columns_) {
    x = i++;
  }
}

ParameterReader::ParameterReader(const std::string &filepath,
                                 std::vector<size_t> columns)
    : file_{filepath, std::ifstream::in}, columns_{columns} {
  if (file_.fail()) {
    throw std::runtime_error("ParameterReader cannot open the input file " +
                             filepath);
  }
  nPoints_ = CountLines(filepath) - 1;
}

ParameterReader::ParameterReader(const std::string &filepath,
                                 std::vector<std::string> columnNames,
                                 bool namedIndexCol)
    : file_{filepath, std::ifstream::in}, columns_{} {
  if (file_.fail()) {
    throw std::runtime_error("ParameterReader cannot open the input file " +
                             filepath);
  }
  nPoints_ = CountLines(filepath) - 1;

  size_t nParam = columnNames.size();
  // read first row and split into words
  std::string temp;
  std::getline(file_, temp);
  std::istringstream ss(temp);
  std::vector<std::string> header{std::istream_iterator<std::string>(ss),
                                  std::istream_iterator<std::string>()};
  // check that size can work out
  if ((header.size() < nParam) ||
      ((header.size() == nParam) && namedIndexCol)) {
    throw std::runtime_error("Cannot parse input file " + filepath +
                             ", invalid column count in header.");
  }
  // drop name of index, if present
  if (namedIndexCol) {
    header.erase(header.begin());
  }
  // find locations of the required names
  auto headstart = header.begin();
  auto headend = header.end();
  for (const auto &name : columnNames) {
    auto it = std::find(headstart, headend, name);
    if (it == headend) {
      throw std::runtime_error("Parameter " + name +
                               " not found in input file " + filepath);
    } else {
      columns_.push_back(it - headstart);
    }
  }
}

bool ParameterReader::GetPoint(std::string &pointID,
                               std::vector<double> &parameters) {
  // get point ID
  std::string bufferID;
  if (std::getline(file_, bufferID, '\t').good()) {
    // get parameters
    parameters.resize(columns_.size());
    std::string line;
    std::getline(file_, line);
    if (file_.good() || file_.eof()) {
      std::istringstream iss{line};
      std::vector<double> row{std::istream_iterator<double>(iss),
                              std::istream_iterator<double>()};
      // save the required rows into the parameters buffer
      std::vector<double> bufferParam(columns_.size());
      for (size_t i = 0; i != columns_.size(); ++i) {
        try {
          bufferParam[i] = row.at(columns_[i]);
        } catch (const std::out_of_range &) {
          throw std::out_of_range("Cannot read parameter at position " +
                                  std::to_string(columns_[i]) +
                                  " from a row of size " +
                                  std::to_string(row.size()));
        }
      }
      // everything worked
      std::swap(bufferID, pointID);
      std::swap(bufferParam, parameters);
      return true;
    }
  }
  return false;
}

size_t ParameterReader::NPoints() const { return nPoints_; }

} // namespace ScannerS::Tools
