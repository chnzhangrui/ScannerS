#include "ScannerS/Tools/ScalarWidths.hpp"
#include "ScannerS/Constants.hpp"

#include <cmath>

namespace ScannerS::ScalarWidths {
double TripleHiggsDec(double mi, double mj, double mk, double coup) {
  using std::pow;
  static constexpr double pi = 3.14159265359;
  if (mi < mj + mk)
    return 0;
  return pow(coup, 2) / 16. / pi / mi *
         std::sqrt(1 - pow(mj + mk, 2) / pow(mi, 2)) *
         std::sqrt(1 - pow(mj - mk, 2) / pow(mi, 2));
}

double TripleHiggsDecEqual(double mi, double mj, double coup) {
  using std::pow;
  static constexpr double pi = 3.14159265359;
  if (mi < 2 * mj)
    return 0;
  return pow(coup, 2) / 32. / pi / mi *
         std::sqrt(1 - 4 * pow(mj, 2) / pow(mi, 2));
}

DataMap::Map SMLikeBRs(const SMWidths &smw, double bsmwidth,
                       const std::string &name) {
  const double wtot = smw.w_h_WW + smw.w_h_ZZ + smw.w_h_bb + smw.w_h_tautau +
                      smw.w_h_gamgam + smw.w_h_gg + smw.w_h_tt + smw.w_h_cc +
                      smw.w_h_ss + smw.w_h_mumu + smw.w_h_Zgam + bsmwidth;
  auto data = DataMap::Map{};
  data["b_" + name + "_WW"] = smw.w_h_WW / wtot;
  data["b_" + name + "_ZZ"] = smw.w_h_ZZ / wtot;
  data["b_" + name + "_bb"] = smw.w_h_bb / wtot;
  data["b_" + name + "_tautau"] = smw.w_h_tautau / wtot;
  data["b_" + name + "_gamgam"] = smw.w_h_gamgam / wtot;
  data["b_" + name + "_gg"] = smw.w_h_gg / wtot;
  data["b_" + name + "_tt"] = smw.w_h_tt / wtot;
  data["b_" + name + "_cc"] = smw.w_h_cc / wtot;
  data["b_" + name + "_ss"] = smw.w_h_ss / wtot;
  data["b_" + name + "_mumu"] = smw.w_h_mumu / wtot;
  data["b_" + name + "_Zgam"] = smw.w_h_Zgam / wtot;
  data["w_" + name] = wtot;
  return data;
}

namespace {
double ReHpLoopAmplitude(double m) {
  using std::pow;
  double exponent = -2.5167211609621996 + 823773.577169314 / pow(m, 3) -
                    24580.043209432348 / pow(m, 2) + 345.9976955175421 / m -
                    0.0006735482945268863 * m;
  return pow(10, exponent);
}
} // namespace

double EffHsmGamGam(double mHp, double ch125HpHm) {
  constexpr double reSmAmp = -6.6081837190822785;
  constexpr double imSmAmp = 4.65351211600309525E-002;
  constexpr double smAmp2 = reSmAmp * reSmAmp + imSmAmp * imSmAmp;
  constexpr double spiraNormalization =
      Constants::vEW / Constants::mZ / Constants::mZ;

  return (pow(reSmAmp + ReHpLoopAmplitude(mHp) * ch125HpHm * spiraNormalization,
              2) +
          pow(imSmAmp, 2)) /
         smAmp2;
}

} // namespace ScannerS::ScalarWidths
