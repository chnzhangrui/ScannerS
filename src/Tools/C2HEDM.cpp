#include "ScannerS/Tools/C2HEDM.hpp"
#include "ScannerS/Constants.hpp"
#include <algorithm>
#include <cmath>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_math.h>
#include <limits>
#include <memory>
#include <stdexcept>

namespace ScannerS::Tools {

namespace {
//! 1st generation fermion masses
constexpr std::array<double, 3> mLightF{Constants::mU, Constants::mD,
                                        Constants::mE};
//! squared 3rd gen fermion masses
constexpr std::array<double, 3> mFsq{Constants::mT * Constants::mT,
                                     Constants::mB *Constants::mB,
                                     Constants::mTau *Constants::mTau};
// arrays of the above quantities, 0 = up-type, 1 = down-type, 2 =
// charged leptons
constexpr std::array<double, 3> Q{Constants::Qu, Constants::Qd,
                                  Constants::Ql}; //< EM quantum numbers
//! fermion couplings to a photon
constexpr std::array<double, 3> c_ffgam{2 * Constants::Qu * Constants::e,
                                        2 * Constants::Qd *Constants::e,
                                        2 * Constants::Ql *Constants::e};
//! up-type quark coupling to Z
constexpr double c_uuZ = Constants::e / Constants::stw / Constants::ctw *
                         (Constants::T3u - 2 * Constants::s2tw * Constants::Qu);
//! down-type quark coupling to Z
constexpr double c_ddZ = Constants::e / Constants::stw / Constants::ctw *
                         (Constants::T3d - 2 * Constants::s2tw * Constants::Qd);
//! charged lepton coupling to Z
constexpr double c_llZ = Constants::e / Constants::stw / Constants::ctw *
                         (Constants::T3l - 2 * Constants::s2tw * Constants::Ql);
//! fermion couplings to a Z
constexpr std::array<double, 3> c_ffZ{c_uuZ, c_ddZ, c_llZ};
constexpr std::array<int, 3> Nc{3, 3, 1}; //< number of colors
} // namespace

namespace C2HEDMDetail {
double fermionLoop(size_t iferm,
                   const std::array<std::array<double, 2>, 3> &c_H_ffj_cpk,
                   double mHsq) {
  using Constants::mZsq;
  double result = 0.;
  for (size_t f = 0; f != 3; f++) { // iterate over top, bottom, tau
    double temp = 0;
    // gammagamma contribution
    temp += c_H_ffj_cpk[iferm][1] * c_H_ffj_cpk[f][0] * c_ffgam[f] * mFsq[f] /
            mHsq * (-1) * funcI1(mFsq[f], mHsq);
    temp += c_H_ffj_cpk[iferm][0] * c_H_ffj_cpk[f][1] * c_ffgam[f] * mFsq[f] /
            mHsq * (-1) * funcI2(mFsq[f], mHsq);
    result += Nc[f] * Q[f] * c_ffgam[iferm] * temp;
    // Zgamma contribution
    temp = 0;
    temp += c_H_ffj_cpk[iferm][1] * c_H_ffj_cpk[f][0] * c_ffZ[f] * mFsq[f] /
            (mHsq - mZsq) * (funcI1(mFsq[f], mZsq) - funcI1(mFsq[f], mHsq));
    temp += c_H_ffj_cpk[iferm][0] * c_H_ffj_cpk[f][1] * c_ffZ[f] * mFsq[f] /
            (mHsq - mZsq) * (funcI2(mFsq[f], mZsq) - funcI2(mFsq[f], mHsq));
    result += Nc[f] * Q[f] * c_ffZ[iferm] * temp;
  }
  return -result * mLightF[iferm] * sqrt(2) * Constants::Gf /
         pow(4 * Constants::pi, 4) * Constants::invGeVToCm;
}

double chargedHiggsLoop(size_t iferm,
                        const std::array<std::array<double, 2>, 3> &c_H_ffj_cpk,
                        const double c_H_HpHm, double mHsq, double mHpsq) {
  using Constants::mZsq;
  using Constants::vEW;
  //! 2HDM H+H-Z coupling
  constexpr double c_HpHmZ{1 / 2. * Constants::e / Constants::stw /
                           Constants::ctw * (1 - 2 * Constants::s2tw)};
  double res = 0;
  // gammagamma contribution
  res += c_ffgam[iferm] * (-1 / 2.) * Constants::e * vEW * vEW / mHsq *
         (-funcI1(mHpsq, mHsq) + funcI2(mHpsq, mHsq));
  // Zgamma contribution
  res += c_ffZ[iferm] * (-1 / 2.) * c_HpHmZ * vEW * vEW / (mHsq - mZsq) *
         (funcI1(mHpsq, mZsq) - funcI1(mHpsq, mHsq) - funcI2(mHpsq, mZsq) +
          funcI2(mHpsq, mHsq));
  res *= c_H_ffj_cpk[iferm][1] * c_H_HpHm;
  return res * mLightF[iferm] * sqrt(2) * Constants::Gf /
         pow(4 * Constants::pi, 4) * Constants::invGeVToCm;
}

double wLoop(size_t iferm,
             const std::array<std::array<double, 2>, 3> &c_H_ffj_cpk,
             const double c_H_VV, const double mHsq) {
  using Constants::mWsq, Constants::mZsq;
  //! WWZ triple gauge coupling
  constexpr double c_WWZ = Constants::e / Constants::stw * Constants::ctw;

  double res = 0;
  // gammagamma contribution
  res += c_ffgam[iferm] * Constants::e * 2 * mWsq / (mHsq) *
         (-1 / 4. * (6 + mHsq / mWsq) * funcI1(mWsq, mHsq) +
          (-4 + 1 / 4. * (6 + mHsq / mWsq)) * funcI2(mWsq, mHsq));
  // Zgamma contribution
  res +=
      c_ffZ[iferm] * c_WWZ * (2 * mWsq) / (mHsq - mZsq) *
      (-1 / 4. * ((6 - mZsq / mWsq) + (1 - mZsq / (2 * mWsq)) * mHsq / mWsq) *
           (funcI1(mWsq, mHsq) - funcI1(mWsq, mZsq)) +
       ((-4 + mZsq / mWsq) +
        1 / 4. * (6 - mZsq / mWsq + (1 - mZsq / (2 * mWsq)) * mHsq / mWsq)) *
           (funcI2(mWsq, mHsq) - funcI2(mWsq, mZsq)));
  return res * c_H_ffj_cpk[iferm][1] * c_H_VV * mLightF[iferm] * sqrt(2) *
         Constants::Gf / pow(4 * Constants::pi, 4) * Constants::invGeVToCm;
}

double hwLoop(size_t iferm,
              const std::array<std::array<double, 2>, 3> &c_H_ffj_cpk,
              const double c_H_VV, const double c_H_HpHm, const double mHsq,
              double mHpsq) {
  using Constants::mWsq;
  constexpr std::array<int, 3> Sx{-1, 1, 1}; //!< Eq. (4.9)
  double res = 0;
  res += c_H_ffj_cpk[iferm][1] * c_H_VV * std::pow(Constants::e, 2) / 2. /
         Constants::s2tw * mWsq / (mHpsq - mWsq) *
         (funcI4(mWsq, mHsq, mHpsq) - funcI4(mHpsq, mHsq, mHpsq));
  res += c_H_ffj_cpk[iferm][1] * c_H_HpHm * mWsq / (mHpsq - mWsq) *
         (funcI5(mWsq, mHsq, mHpsq) - funcI5(mHpsq, mHsq, mHpsq));
  return -res * Sx[iferm] * mLightF[iferm] * sqrt(2) * Constants::Gf /
         pow(4 * Constants::pi, 4) * Constants::invGeVToCm;
}

namespace {
using GSLIntegrationWorkspace =
    std::unique_ptr<gsl_integration_workspace,
                    decltype(&gsl_integration_workspace_free)>;

using GSLBackupIntegrationWorkspace =
    std::unique_ptr<gsl_integration_cquad_workspace,
                    decltype(&gsl_integration_cquad_workspace_free)>;

double gslIntegrateFunc(const gsl_function &f, double a, double b) {
  static GSLIntegrationWorkspace integrator = GSLIntegrationWorkspace{
      gsl_integration_workspace_alloc(100), gsl_integration_workspace_free};
  static GSLBackupIntegrationWorkspace backupIntegrator =
      GSLBackupIntegrationWorkspace{gsl_integration_cquad_workspace_alloc(100),
                                    gsl_integration_cquad_workspace_free};

  double result, error;
  // toggle gsl error handler off
  gsl_error_handler_t *backup_handler = gsl_set_error_handler_off();
  // perform the integration using an adaptive method
  int status = gsl_integration_qags(
      &f, a, b, 100 * std::numeric_limits<double>::epsilon(), 1e-10, 100,
      integrator.get(), &result, &error);

  // try again using a more robust method
  if (status != GSL_SUCCESS) {
    std::size_t nEvals;
    status = gsl_integration_cquad(
        &f, a, b, 100 * std::numeric_limits<double>::epsilon(), 1e-7,
        backupIntegrator.get(), &result, &error, &nEvals);
  }
  // toggle error handler back on
  gsl_set_error_handler(backup_handler);
  // if something went wrong, return inf
  if (status != GSL_SUCCESS) {
    return std::numeric_limits<double>::infinity();
  }
  return result;
}
} // namespace

double funcI1(double m1sq, double m2sq) {
  struct Iparam {
    double m1sq;
    double m2sq;
  } iparam{m1sq, m2sq};

  gsl_function f;
  f.function = [](double x, void *params) {
    Iparam *A{static_cast<Iparam *>(params)};
    return (1 - 2 * x * (1 - x)) *
           (A->m2sq / (A->m1sq - A->m2sq * x * (1 - x)) *
            log(A->m2sq * x * (1 - x) / A->m1sq));
  };
  f.params = &iparam;
  return gslIntegrateFunc(f, 0, 1);
}

double funcI2(double m1sq, double m2sq) {
  struct Iparam {
    double m1sq;
    double m2sq;
  } iparam{m1sq, m2sq};

  gsl_function f;
  f.function = [](double x, void *params) {
    Iparam *A{static_cast<Iparam *>(params)};
    return A->m2sq / (A->m1sq - A->m2sq * x * (1 - x)) *
           log(A->m2sq * x * (1 - x) / A->m1sq);
  };
  f.params = &iparam;
  return gslIntegrateFunc(f, 0, 1);
}

double funcI4(double m1sq, double m2sq, double mHpsq) {
  struct Iparam {
    double m1sq;
    double m2sq;
    double mHpsq;
    double mWsq;
  } iparam{m1sq, m2sq, mHpsq, Constants::mWsq};

  gsl_function f;
  f.function = [](double x, void *params) {
    Iparam *A{static_cast<Iparam *>(params)};
    return (x * pow(1 - x, 2) - 4 * pow(1 - x, 2) +
            (A->mHpsq - A->m2sq) / A->mWsq * x * pow(1 - x, 2)) *
           (A->m1sq /
            (A->mWsq * (1 - x) + A->m2sq * x - A->m1sq * x * (1 - x))) *
           log((A->mWsq * (1 - x) + A->m2sq * x) / (A->m1sq * x * (1 - x)));
  };
  f.params = &iparam;
  return gslIntegrateFunc(f, 0, 1);
}

double funcI5(double m1sq, double m2sq, double mHpsq) {
  struct Iparam {
    double m1sq;
    double m2sq;
    double mHpsq;
    double mWsq;
  } iparam{m1sq, m2sq, mHpsq, Constants::mWsq};

  gsl_function f;
  f.function = [](double x, void *params) {
    Iparam *A{static_cast<Iparam *>(params)};
    return 2 * (A->m1sq * x * pow(1 - x, 2)) /
           (A->mHpsq * (1 - x) + A->m2sq * x - A->m1sq * x * (1 - x)) *
           log((A->mHpsq * (1 - x) + A->m2sq * x) / (A->m1sq * x * (1 - x)));
  };
  f.params = &iparam;
  return gslIntegrateFunc(f, 0, 1);
}
} // namespace C2HEDMDetail

} // namespace ScannerS::Tools
